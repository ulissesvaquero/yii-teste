<?php
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;

    $dataProvider = new ArrayDataProvider([
        'allModels' => $model->experienciaProfissionals,
        'key' => 'id'
    ]);
    $gridColumns = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        'nome_cargo',
        'dt_inicio',
        'dt_fim',
        'valor_salario_anual',
        [
                'attribute' => 'tipoSalario.id',
                'label' => 'Tipo Salario'
            ],
        'nome_supervisor',
        'nome_empregador',
        'site_empresa',
        'descricao_tarefa:ntext',
        'descricao_resultado:ntext',
        'descricao_motivo_saida',
        'contacta_empregador',
        'is_emprego_atual',
        'qtd_pessoa_supervisionou',
        [
            'class' => 'yii\grid\ActionColumn',
            'controller' => 'experiencia-profissional'
        ],
    ];
    
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'containerOptions' => ['style' => 'overflow: auto'],
        'pjax' => true,
        'beforeHeader' => [
            [
                'options' => ['class' => 'skip-export']
            ]
        ],
        'export' => [
            'fontAwesome' => true
        ],
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'showPageSummary' => false,
        'persistResize' => false,
    ]);
