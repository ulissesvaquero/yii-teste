<?php
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;

    $dataProvider = new ArrayDataProvider([
        'allModels' => $model->pessoaAreaAtuacaos,
        'key' => 'id'
    ]);
    $gridColumns = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        [
                'attribute' => 'nivelExperiencia.id',
                'label' => 'Nivel Experiencia'
            ],
        [
                'attribute' => 'especializacao.id',
                'label' => 'Especializacao'
            ],
        [
                'attribute' => 'anoExperiencia.id',
                'label' => 'Ano Experiencia'
            ],
        [
            'class' => 'yii\grid\ActionColumn',
            'controller' => 'pessoa-area-atuacao'
        ],
    ];
    
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'containerOptions' => ['style' => 'overflow: auto'],
        'pjax' => true,
        'beforeHeader' => [
            [
                'options' => ['class' => 'skip-export']
            ]
        ],
        'export' => [
            'fontAwesome' => true
        ],
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'showPageSummary' => false,
        'persistResize' => false,
    ]);
