<div class="form-group" id="add-experiencia-profissional">
<?php
use kartik\grid\GridView;
use kartik\builder\TabularForm;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use yii\widgets\Pjax;

$dataProvider = new ArrayDataProvider([
    'allModels' => $row,
    'pagination' => [
        'pageSize' => -1
    ]
]);
echo TabularForm::widget([
    'dataProvider' => $dataProvider,
    'formName' => 'ExperienciaProfissional',
    'checkboxColumn' => false,
    'actionColumn' => false,
    'attributeDefaults' => [
        'type' => TabularForm::INPUT_TEXT,
    ],
    'attributes' => [
        "id" => ['type' => TabularForm::INPUT_HIDDEN, 'visible' => false],
        'nome_cargo' => ['type' => TabularForm::INPUT_TEXT],
        'dt_inicio' => ['type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\datecontrol\DateControl::classname(),
            'options' => [
                'type' => \kartik\datecontrol\DateControl::FORMAT_DATE,
                'saveFormat' => 'php:Y-m-d',
                'ajaxConversion' => true,
                'options' => [
                    'pluginOptions' => [
                        'placeholder' => 'Choose Dt Inicio',
                        'autoclose' => true
                    ]
                ],
            ]
        ],
        'dt_fim' => ['type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\datecontrol\DateControl::classname(),
            'options' => [
                'type' => \kartik\datecontrol\DateControl::FORMAT_DATE,
                'saveFormat' => 'php:Y-m-d',
                'ajaxConversion' => true,
                'options' => [
                    'pluginOptions' => [
                        'placeholder' => 'Choose Dt Fim',
                        'autoclose' => true
                    ]
                ],
            ]
        ],
        'valor_salario_anual' => ['type' => TabularForm::INPUT_TEXT],
        'tipo_salario_id' => [
            'label' => 'Tipo salario',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\app\models\TipoSalario::find()->orderBy('id')->asArray()->all(), 'id', 'id'),
                'options' => ['placeholder' => 'Choose Tipo salario'],
            ],
            'columnOptions' => ['width' => '200px']
        ],
        'nome_supervisor' => ['type' => TabularForm::INPUT_TEXT],
        'nome_empregador' => ['type' => TabularForm::INPUT_TEXT],
        'site_empresa' => ['type' => TabularForm::INPUT_TEXT],
        'descricao_tarefa' => ['type' => TabularForm::INPUT_TEXTAREA],
        'descricao_resultado' => ['type' => TabularForm::INPUT_TEXTAREA],
        'descricao_motivo_saida' => ['type' => TabularForm::INPUT_TEXT],
        'contacta_empregador' => ['type' => TabularForm::INPUT_CHECKBOX,
            'options' => [
                'style' => 'position : relative; margin-top : -9px'
            ]
        ],
        'is_emprego_atual' => ['type' => TabularForm::INPUT_CHECKBOX,
            'options' => [
                'style' => 'position : relative; margin-top : -9px'
            ]
        ],
        'qtd_pessoa_supervisionou' => ['type' => TabularForm::INPUT_TEXT],
        'del' => [
            'type' => 'raw',
            'label' => '',
            'value' => function($model, $key) {
                return Html::a('<i class="glyphicon glyphicon-trash"></i>', '#', ['title' =>  'Delete', 'onClick' => 'delRowExperienciaProfissional(' . $key . '); return false;', 'id' => 'experiencia-profissional-del-btn']);
            },
        ],
    ],
    'gridSettings' => [
        'panel' => [
            'heading' => false,
            'type' => GridView::TYPE_DEFAULT,
            'before' => false,
            'footer' => false,
            'after' => Html::button('<i class="glyphicon glyphicon-plus"></i>' . 'Add Experiencia Profissional', ['type' => 'button', 'class' => 'btn btn-success kv-batch-create', 'onClick' => 'addRowExperienciaProfissional()']),
        ]
    ]
]);
echo  "    </div>\n\n";
?>

