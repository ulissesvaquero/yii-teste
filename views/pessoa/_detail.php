<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Pessoa */

?>
<div class="pessoa-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Html::encode($model->id) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'nome',
        'sobrenome',
        'dt_nascimento',
        'telefone_1',
        'telefone_2',
        'email:email',
        'url_blog:url',
        'url_linkedin:url',
        'url_site:url',
        'twitter',
        'url_facebook:url',
        'skype',
        'url_foto:url',
        [
            'attribute' => 'titulo.id',
            'label' => 'Titulo',
        ],
        [
            'attribute' => 'estadoCivil.id',
            'label' => 'Estado Civil',
        ],
        [
            'attribute' => 'sexo.id',
            'label' => 'Sexo',
        ],
        [
            'attribute' => 'usuario.id',
            'label' => 'Usuario',
        ],
        [
            'attribute' => 'endereco.id',
            'label' => 'Endereco',
        ],
        [
            'attribute' => 'tipoServicoPublico.id',
            'label' => 'Tipo Servico Publico',
        ],
        'vinculo_servico_publico',
        [
            'attribute' => 'tipoDedicacao.id',
            'label' => 'Tipo Dedicacao',
        ],
        'nome_orgao_publico',
        [
            'attribute' => 'situacaoContratual.id',
            'label' => 'Situacao Contratual',
        ],
        [
            'attribute' => 'familiarOnu.id',
            'label' => 'Familiar Onu',
        ],
        'id_econtracting_antigo',
        'trabalha_atualmente_onu',
        'trabalha_atualmente_opas',
        'qualificacao_info:ntext',
        'publicacao:ntext',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>