<div class="form-group" id="add-oportunidade-aplicada">
<?php
use kartik\grid\GridView;
use kartik\builder\TabularForm;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use yii\widgets\Pjax;

$dataProvider = new ArrayDataProvider([
    'allModels' => $row,
    'pagination' => [
        'pageSize' => -1
    ]
]);
echo TabularForm::widget([
    'dataProvider' => $dataProvider,
    'formName' => 'OportunidadeAplicada',
    'checkboxColumn' => false,
    'actionColumn' => false,
    'attributeDefaults' => [
        'type' => TabularForm::INPUT_TEXT,
    ],
    'attributes' => [
        "id" => ['type' => TabularForm::INPUT_HIDDEN, 'visible' => false],
        'data_aplicacacao' => ['type' => TabularForm::INPUT_TEXT],
        'oportunidade_id' => [
            'label' => 'Oportunidade',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\app\models\Oportunidade::find()->orderBy('id')->asArray()->all(), 'id', 'id'),
                'options' => ['placeholder' => 'Choose Oportunidade'],
            ],
            'columnOptions' => ['width' => '200px']
        ],
        'is_aprovado' => ['type' => TabularForm::INPUT_CHECKBOX,
            'options' => [
                'style' => 'position : relative; margin-top : -9px'
            ]
        ],
        'score' => ['type' => TabularForm::INPUT_TEXT],
        'carta_apresentacao' => ['type' => TabularForm::INPUT_TEXTAREA],
        'assinatura' => ['type' => TabularForm::INPUT_TEXT],
        'del' => [
            'type' => 'raw',
            'label' => '',
            'value' => function($model, $key) {
                return Html::a('<i class="glyphicon glyphicon-trash"></i>', '#', ['title' =>  'Delete', 'onClick' => 'delRowOportunidadeAplicada(' . $key . '); return false;', 'id' => 'oportunidade-aplicada-del-btn']);
            },
        ],
    ],
    'gridSettings' => [
        'panel' => [
            'heading' => false,
            'type' => GridView::TYPE_DEFAULT,
            'before' => false,
            'footer' => false,
            'after' => Html::button('<i class="glyphicon glyphicon-plus"></i>' . 'Add Oportunidade Aplicada', ['type' => 'button', 'class' => 'btn btn-success kv-batch-create', 'onClick' => 'addRowOportunidadeAplicada()']),
        ]
    ]
]);
echo  "    </div>\n\n";
?>

