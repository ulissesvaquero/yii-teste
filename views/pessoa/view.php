<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Pessoa */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Pessoas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pessoa-view">

    <div class="row">
        <div class="col-sm-8">
            <h2><?= 'Pessoa'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-4" style="margin-top: 15px">
<?=             
             Html::a('<i class="fa glyphicon glyphicon-hand-up"></i> ' . 'PDF', 
                ['pdf', 'id' => $model->id],
                [
                    'class' => 'btn btn-danger',
                    'target' => '_blank',
                    'data-toggle' => 'tooltip',
                    'title' => 'Will open the generated PDF file in a new window'
                ]
            )?>
            <?= Html::a('Save As New', ['save-as-new', 'id' => $model->id], ['class' => 'btn btn-info']) ?>            
            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'nome',
        'sobrenome',
        'dt_nascimento',
        'telefone_1',
        'telefone_2',
        'email:email',
        'url_blog:url',
        'url_linkedin:url',
        'url_site:url',
        'twitter',
        'url_facebook:url',
        'skype',
        'url_foto:url',
        [
            'attribute' => 'titulo.id',
            'label' => 'Titulo',
        ],
        [
            'attribute' => 'estadoCivil.id',
            'label' => 'Estado Civil',
        ],
        [
            'attribute' => 'sexo.id',
            'label' => 'Sexo',
        ],
        [
            'attribute' => 'usuario.id',
            'label' => 'Usuario',
        ],
        [
            'attribute' => 'endereco.id',
            'label' => 'Endereco',
        ],
        [
            'attribute' => 'tipoServicoPublico.id',
            'label' => 'Tipo Servico Publico',
        ],
        'vinculo_servico_publico',
        [
            'attribute' => 'tipoDedicacao.id',
            'label' => 'Tipo Dedicacao',
        ],
        'nome_orgao_publico',
        [
            'attribute' => 'situacaoContratual.id',
            'label' => 'Situacao Contratual',
        ],
        [
            'attribute' => 'familiarOnu.id',
            'label' => 'Familiar Onu',
        ],
        'id_econtracting_antigo',
        'trabalha_atualmente_onu',
        'trabalha_atualmente_opas',
        'qualificacao_info:ntext',
        'publicacao:ntext',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
    
    <div class="row">
<?php
if($providerAcessoPessoaMenu->totalCount){
    $gridColumnAcessoPessoaMenu = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
            [
                'attribute' => 'menu.id',
                'label' => 'Menu'
            ],
                        'data_atualizacao',
            'is_informado',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerAcessoPessoaMenu,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-acesso-pessoa-menu']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Acesso Pessoa Menu'),
        ],
        'columns' => $gridColumnAcessoPessoaMenu
    ]);
}
?>
    </div>
    
    <div class="row">
<?php
if($providerAvaliador->totalCount){
    $gridColumnAvaliador = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
                        [
                'attribute' => 'tipoAvaliador.id',
                'label' => 'Tipo Avaliador'
            ],
            [
                'attribute' => 'oportunidade.id',
                'label' => 'Oportunidade'
            ],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerAvaliador,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-avaliador']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Avaliador'),
        ],
        'columns' => $gridColumnAvaliador
    ]);
}
?>
    </div>
    
    <div class="row">
<?php
if($providerExperienciaProfissional->totalCount){
    $gridColumnExperienciaProfissional = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
            'nome_cargo',
            'dt_inicio',
            'dt_fim',
            'valor_salario_anual',
            [
                'attribute' => 'tipoSalario.id',
                'label' => 'Tipo Salario'
            ],
            'nome_supervisor',
            'nome_empregador',
            'site_empresa',
            'descricao_tarefa:ntext',
            'descricao_resultado:ntext',
            'descricao_motivo_saida',
            'contacta_empregador',
                        'is_emprego_atual',
            'qtd_pessoa_supervisionou',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerExperienciaProfissional,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-experiencia-profissional']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Experiencia Profissional'),
        ],
        'columns' => $gridColumnExperienciaProfissional
    ]);
}
?>
    </div>
    
    <div class="row">
<?php
if($providerExperienciaProfissionalInternacional->totalCount){
    $gridColumnExperienciaProfissionalInternacional = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
            'nome_organizacao',
            'lotacao',
            'is_empregado',
            'dt_inicio',
            'dt_fim',
            [
                'attribute' => 'tipoContratoInternacional.id',
                'label' => 'Tipo Contrato Internacional'
            ],
                ];
    echo Gridview::widget([
        'dataProvider' => $providerExperienciaProfissionalInternacional,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-experiencia-profissional-internacional']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Experiencia Profissional Internacional'),
        ],
        'columns' => $gridColumnExperienciaProfissionalInternacional
    ]);
}
?>
    </div>
    
    <div class="row">
<?php
if($providerHabilidadeInformatica->totalCount){
    $gridColumnHabilidadeInformatica = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
                        [
                'attribute' => 'ferramentaInformatica.id',
                'label' => 'Ferramenta Informatica'
            ],
            [
                'attribute' => 'nivelInformatica.id',
                'label' => 'Nivel Informatica'
            ],
            'descricao:ntext',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerHabilidadeInformatica,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-habilidade-informatica']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Habilidade Informatica'),
        ],
        'columns' => $gridColumnHabilidadeInformatica
    ]);
}
?>
    </div>
    
    <div class="row">
<?php
if($providerOportunidadeAplicada->totalCount){
    $gridColumnOportunidadeAplicada = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
            'data_aplicacacao',
                        [
                'attribute' => 'oportunidade.id',
                'label' => 'Oportunidade'
            ],
            'is_aprovado',
            'score',
            'carta_apresentacao:ntext',
            'assinatura',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerOportunidadeAplicada,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-oportunidade-aplicada']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Oportunidade Aplicada'),
        ],
        'columns' => $gridColumnOportunidadeAplicada
    ]);
}
?>
    </div>
    
    <div class="row">
<?php
if($providerPessoaAreaAtuacao->totalCount){
    $gridColumnPessoaAreaAtuacao = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
            [
                'attribute' => 'nivelExperiencia.id',
                'label' => 'Nivel Experiencia'
            ],
            [
                'attribute' => 'especializacao.id',
                'label' => 'Especializacao'
            ],
            [
                'attribute' => 'anoExperiencia.id',
                'label' => 'Ano Experiencia'
            ],
                ];
    echo Gridview::widget([
        'dataProvider' => $providerPessoaAreaAtuacao,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-pessoa-area-atuacao']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Pessoa Area Atuacao'),
        ],
        'columns' => $gridColumnPessoaAreaAtuacao
    ]);
}
?>
    </div>
    
    <div class="row">
<?php
if($providerPessoaEscolaridade->totalCount){
    $gridColumnPessoaEscolaridade = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
            'curso',
            'nome_instituicao',
            'dt_inicio',
            'dt_fim',
            [
                'attribute' => 'situacaoCurso.id',
                'label' => 'Situacao Curso'
            ],
            [
                'attribute' => 'escolaridade.id',
                'label' => 'Escolaridade'
            ],
            'estado',
            'cidade',
                        [
                'attribute' => 'pais.id',
                'label' => 'Pais'
            ],
            [
                'attribute' => 'cursoFormacao.id',
                'label' => 'Curso Formacao'
            ],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerPessoaEscolaridade,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-pessoa-escolaridade']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Pessoa Escolaridade'),
        ],
        'columns' => $gridColumnPessoaEscolaridade
    ]);
}
?>
    </div>
    
    <div class="row">
<?php
if($providerPessoaIdioma->totalCount){
    $gridColumnPessoaIdioma = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
                        [
                'attribute' => 'nivelOral.id',
                'label' => 'Nivel Oral'
            ],
            [
                'attribute' => 'nivelLeitura.id',
                'label' => 'Nivel Leitura'
            ],
            [
                'attribute' => 'nivelEscrita.id',
                'label' => 'Nivel Escrita'
            ],
            [
                'attribute' => 'idioma.id',
                'label' => 'Idioma'
            ],
            [
                'attribute' => 'infoAdicionalIdioma.id',
                'label' => 'Info Adicional Idioma'
            ],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerPessoaIdioma,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-pessoa-idioma']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Pessoa Idioma'),
        ],
        'columns' => $gridColumnPessoaIdioma
    ]);
}
?>
    </div>
    
    <div class="row">
<?php
if($providerPessoaNacionalidade->totalCount){
    $gridColumnPessoaNacionalidade = [
        ['class' => 'yii\grid\SerialColumn'],
                        [
                'attribute' => 'pais.id',
                'label' => 'Pais'
            ],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerPessoaNacionalidade,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-pessoa-nacionalidade']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Pessoa Nacionalidade'),
        ],
        'columns' => $gridColumnPessoaNacionalidade
    ]);
}
?>
    </div>
    
    <div class="row">
<?php
if($providerPessoaStorage->totalCount){
    $gridColumnPessoaStorage = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
                        [
                'attribute' => 'tipoDadoStorage.id',
                'label' => 'Tipo Dado Storage'
            ],
            'url:url',
            'descricao:ntext',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerPessoaStorage,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-pessoa-storage']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Pessoa Storage'),
        ],
        'columns' => $gridColumnPessoaStorage
    ]);
}
?>
    </div>
    
    <div class="row">
<?php
if($providerPessoaTipoContrato->totalCount){
    $gridColumnPessoaTipoContrato = [
        ['class' => 'yii\grid\SerialColumn'],
                        [
                'attribute' => 'tipoContrato.id',
                'label' => 'Tipo Contrato'
            ],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerPessoaTipoContrato,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-pessoa-tipo-contrato']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Pessoa Tipo Contrato'),
        ],
        'columns' => $gridColumnPessoaTipoContrato
    ]);
}
?>
    </div>
    
    <div class="row">
<?php
if($providerReferencia->totalCount){
    $gridColumnReferencia = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
            'nome',
            'endereco',
            'email:email',
            'telefone',
            'cargo',
            'empresa',
            'tipo_vinculo',
            'contacta_referencia',
                ];
    echo Gridview::widget([
        'dataProvider' => $providerReferencia,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-referencia']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Referencia'),
        ],
        'columns' => $gridColumnReferencia
    ]);
}
?>
    </div>
</div>
