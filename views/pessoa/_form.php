<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Pessoa */
/* @var $form yii\widgets\ActiveForm */

\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'AcessoPessoaMenu', 
        'relID' => 'acesso-pessoa-menu', 
        'value' => \yii\helpers\Json::encode($model->acessoPessoaMenus),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'Avaliador', 
        'relID' => 'avaliador', 
        'value' => \yii\helpers\Json::encode($model->avaliadors),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'ExperienciaProfissional', 
        'relID' => 'experiencia-profissional', 
        'value' => \yii\helpers\Json::encode($model->experienciaProfissionals),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'ExperienciaProfissionalInternacional', 
        'relID' => 'experiencia-profissional-internacional', 
        'value' => \yii\helpers\Json::encode($model->experienciaProfissionalInternacionals),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'HabilidadeInformatica', 
        'relID' => 'habilidade-informatica', 
        'value' => \yii\helpers\Json::encode($model->habilidadeInformaticas),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'OportunidadeAplicada', 
        'relID' => 'oportunidade-aplicada', 
        'value' => \yii\helpers\Json::encode($model->oportunidadeAplicadas),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'PessoaAreaAtuacao', 
        'relID' => 'pessoa-area-atuacao', 
        'value' => \yii\helpers\Json::encode($model->pessoaAreaAtuacaos),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'PessoaEscolaridade', 
        'relID' => 'pessoa-escolaridade', 
        'value' => \yii\helpers\Json::encode($model->pessoaEscolaridades),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'PessoaIdioma', 
        'relID' => 'pessoa-idioma', 
        'value' => \yii\helpers\Json::encode($model->pessoaIdiomas),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'PessoaNacionalidade', 
        'relID' => 'pessoa-nacionalidade', 
        'value' => \yii\helpers\Json::encode($model->pessoaNacionalidades),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'PessoaStorage', 
        'relID' => 'pessoa-storage', 
        'value' => \yii\helpers\Json::encode($model->pessoaStorages),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'PessoaTipoContrato', 
        'relID' => 'pessoa-tipo-contrato', 
        'value' => \yii\helpers\Json::encode($model->pessoaTipoContratos),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'Referencia', 
        'relID' => 'referencia', 
        'value' => \yii\helpers\Json::encode($model->referencias),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
?>

<div class="pessoa-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?= $form->field($model, 'nome')->textInput(['maxlength' => true, 'placeholder' => 'Nome']) ?>

    <?= $form->field($model, 'sobrenome')->textInput(['maxlength' => true, 'placeholder' => 'Sobrenome']) ?>

    <?= $form->field($model, 'dt_nascimento')->widget(\kartik\datecontrol\DateControl::classname(), [
        'type' => \kartik\datecontrol\DateControl::FORMAT_DATE,
        'saveFormat' => 'php:Y-m-d',
        'ajaxConversion' => true,
        'options' => [
            'pluginOptions' => [
                'placeholder' => 'Choose Dt Nascimento',
                'autoclose' => true
            ]
        ],
    ]); ?>

    <?= $form->field($model, 'telefone_1')->textInput(['maxlength' => true, 'placeholder' => 'Telefone 1']) ?>

    <?= $form->field($model, 'telefone_2')->textInput(['maxlength' => true, 'placeholder' => 'Telefone 2']) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true, 'placeholder' => 'Email']) ?>

    <?= $form->field($model, 'url_blog')->textInput(['maxlength' => true, 'placeholder' => 'Url Blog']) ?>

    <?= $form->field($model, 'url_linkedin')->textInput(['maxlength' => true, 'placeholder' => 'Url Linkedin']) ?>

    <?= $form->field($model, 'url_site')->textInput(['maxlength' => true, 'placeholder' => 'Url Site']) ?>

    <?= $form->field($model, 'twitter')->textInput(['maxlength' => true, 'placeholder' => 'Twitter']) ?>

    <?= $form->field($model, 'url_facebook')->textInput(['maxlength' => true, 'placeholder' => 'Url Facebook']) ?>

    <?= $form->field($model, 'skype')->textInput(['maxlength' => true, 'placeholder' => 'Skype']) ?>

    <?= $form->field($model, 'url_foto')->textInput(['maxlength' => true, 'placeholder' => 'Url Foto']) ?>

    <?= $form->field($model, 'titulo_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\Titulo::find()->orderBy('id')->asArray()->all(), 'id', 'id'),
        'options' => ['placeholder' => 'Choose Titulo'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'estado_civil_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\EstadoCivil::find()->orderBy('id')->asArray()->all(), 'id', 'id'),
        'options' => ['placeholder' => 'Choose Estado civil'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'sexo_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\Sexo::find()->orderBy('id')->asArray()->all(), 'id', 'id'),
        'options' => ['placeholder' => 'Choose Sexo'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'usuario_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\Usuario::find()->orderBy('id')->asArray()->all(), 'id', 'id'),
        'options' => ['placeholder' => 'Choose Usuario'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'endereco_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\Endereco::find()->orderBy('id')->asArray()->all(), 'id', 'id'),
        'options' => ['placeholder' => 'Choose Endereco'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'tipo_servico_publico_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\TipoServicoPublico::find()->orderBy('id')->asArray()->all(), 'id', 'id'),
        'options' => ['placeholder' => 'Choose Tipo servico publico'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'vinculo_servico_publico')->checkbox() ?>

    <?= $form->field($model, 'tipo_dedicacao_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\TipoDedicacao::find()->orderBy('id')->asArray()->all(), 'id', 'id'),
        'options' => ['placeholder' => 'Choose Tipo dedicacao'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'nome_orgao_publico')->textInput(['maxlength' => true, 'placeholder' => 'Nome Orgao Publico']) ?>

    <?= $form->field($model, 'situacao_contratual_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\SituacaoContratual::find()->orderBy('id')->asArray()->all(), 'id', 'id'),
        'options' => ['placeholder' => 'Choose Situacao contratual'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'familiar_onu_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\FamiliarOnu::find()->orderBy('id')->asArray()->all(), 'id', 'id'),
        'options' => ['placeholder' => 'Choose Familiar onu'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'id_econtracting_antigo')->textInput(['placeholder' => 'Id Econtracting Antigo']) ?>

    <?= $form->field($model, 'trabalha_atualmente_onu')->checkbox() ?>

    <?= $form->field($model, 'trabalha_atualmente_opas')->checkbox() ?>

    <?= $form->field($model, 'qualificacao_info')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'publicacao')->textarea(['rows' => 6]) ?>

    <?php
    $forms = [
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('AcessoPessoaMenu'),
            'content' => $this->render('_formAcessoPessoaMenu', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->acessoPessoaMenus),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Avaliador'),
            'content' => $this->render('_formAvaliador', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->avaliadors),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('ExperienciaProfissional'),
            'content' => $this->render('_formExperienciaProfissional', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->experienciaProfissionals),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('ExperienciaProfissionalInternacional'),
            'content' => $this->render('_formExperienciaProfissionalInternacional', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->experienciaProfissionalInternacionals),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('HabilidadeInformatica'),
            'content' => $this->render('_formHabilidadeInformatica', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->habilidadeInformaticas),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('OportunidadeAplicada'),
            'content' => $this->render('_formOportunidadeAplicada', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->oportunidadeAplicadas),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('PessoaAreaAtuacao'),
            'content' => $this->render('_formPessoaAreaAtuacao', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->pessoaAreaAtuacaos),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('PessoaEscolaridade'),
            'content' => $this->render('_formPessoaEscolaridade', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->pessoaEscolaridades),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('PessoaIdioma'),
            'content' => $this->render('_formPessoaIdioma', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->pessoaIdiomas),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('PessoaNacionalidade'),
            'content' => $this->render('_formPessoaNacionalidade', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->pessoaNacionalidades),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('PessoaStorage'),
            'content' => $this->render('_formPessoaStorage', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->pessoaStorages),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('PessoaTipoContrato'),
            'content' => $this->render('_formPessoaTipoContrato', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->pessoaTipoContratos),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Referencia'),
            'content' => $this->render('_formReferencia', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->referencias),
            ]),
        ],
    ];
    echo kartik\tabs\TabsX::widget([
        'items' => $forms,
        'position' => kartik\tabs\TabsX::POS_ABOVE,
        'encodeLabels' => false,
        'pluginOptions' => [
            'bordered' => true,
            'sideways' => true,
            'enableCache' => false,
        ],
    ]);
    ?>
    <div class="form-group">
    <?php if(Yii::$app->controller->action->id != 'save-as-new'): ?>
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    <?php endif; ?>
    <?php if(Yii::$app->controller->action->id != 'create'): ?>
        <?= Html::submitButton('Save As New', ['class' => 'btn btn-info', 'value' => '1', 'name' => '_asnew']) ?>
    <?php endif; ?>
        <?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
