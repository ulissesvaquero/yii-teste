<div class="form-group" id="add-referencia">
<?php
use kartik\grid\GridView;
use kartik\builder\TabularForm;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use yii\widgets\Pjax;

$dataProvider = new ArrayDataProvider([
    'allModels' => $row,
    'pagination' => [
        'pageSize' => -1
    ]
]);
echo TabularForm::widget([
    'dataProvider' => $dataProvider,
    'formName' => 'Referencia',
    'checkboxColumn' => false,
    'actionColumn' => false,
    'attributeDefaults' => [
        'type' => TabularForm::INPUT_TEXT,
    ],
    'attributes' => [
        "id" => ['type' => TabularForm::INPUT_HIDDEN, 'visible' => false],
        'nome' => ['type' => TabularForm::INPUT_TEXT],
        'endereco' => ['type' => TabularForm::INPUT_TEXT],
        'email' => ['type' => TabularForm::INPUT_TEXT],
        'telefone' => ['type' => TabularForm::INPUT_TEXT],
        'cargo' => ['type' => TabularForm::INPUT_TEXT],
        'empresa' => ['type' => TabularForm::INPUT_TEXT],
        'tipo_vinculo' => ['type' => TabularForm::INPUT_TEXT],
        'contacta_referencia' => ['type' => TabularForm::INPUT_CHECKBOX,
            'options' => [
                'style' => 'position : relative; margin-top : -9px'
            ]
        ],
        'del' => [
            'type' => 'raw',
            'label' => '',
            'value' => function($model, $key) {
                return Html::a('<i class="glyphicon glyphicon-trash"></i>', '#', ['title' =>  'Delete', 'onClick' => 'delRowReferencia(' . $key . '); return false;', 'id' => 'referencia-del-btn']);
            },
        ],
    ],
    'gridSettings' => [
        'panel' => [
            'heading' => false,
            'type' => GridView::TYPE_DEFAULT,
            'before' => false,
            'footer' => false,
            'after' => Html::button('<i class="glyphicon glyphicon-plus"></i>' . 'Add Referencia', ['type' => 'button', 'class' => 'btn btn-success kv-batch-create', 'onClick' => 'addRowReferencia()']),
        ]
    ]
]);
echo  "    </div>\n\n";
?>

