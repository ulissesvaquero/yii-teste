<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PessoaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-pessoa-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?= $form->field($model, 'nome')->textInput(['maxlength' => true, 'placeholder' => 'Nome']) ?>

    <?= $form->field($model, 'sobrenome')->textInput(['maxlength' => true, 'placeholder' => 'Sobrenome']) ?>

    <?= $form->field($model, 'dt_nascimento')->widget(\kartik\datecontrol\DateControl::classname(), [
        'type' => \kartik\datecontrol\DateControl::FORMAT_DATE,
        'saveFormat' => 'php:Y-m-d',
        'ajaxConversion' => true,
        'options' => [
            'pluginOptions' => [
                'placeholder' => 'Choose Dt Nascimento',
                'autoclose' => true
            ]
        ],
    ]); ?>

    <?= $form->field($model, 'telefone_1')->textInput(['maxlength' => true, 'placeholder' => 'Telefone 1']) ?>

    <?php /* echo $form->field($model, 'telefone_2')->textInput(['maxlength' => true, 'placeholder' => 'Telefone 2']) */ ?>

    <?php /* echo $form->field($model, 'email')->textInput(['maxlength' => true, 'placeholder' => 'Email']) */ ?>

    <?php /* echo $form->field($model, 'url_blog')->textInput(['maxlength' => true, 'placeholder' => 'Url Blog']) */ ?>

    <?php /* echo $form->field($model, 'url_linkedin')->textInput(['maxlength' => true, 'placeholder' => 'Url Linkedin']) */ ?>

    <?php /* echo $form->field($model, 'url_site')->textInput(['maxlength' => true, 'placeholder' => 'Url Site']) */ ?>

    <?php /* echo $form->field($model, 'twitter')->textInput(['maxlength' => true, 'placeholder' => 'Twitter']) */ ?>

    <?php /* echo $form->field($model, 'url_facebook')->textInput(['maxlength' => true, 'placeholder' => 'Url Facebook']) */ ?>

    <?php /* echo $form->field($model, 'skype')->textInput(['maxlength' => true, 'placeholder' => 'Skype']) */ ?>

    <?php /* echo $form->field($model, 'url_foto')->textInput(['maxlength' => true, 'placeholder' => 'Url Foto']) */ ?>

    <?php /* echo $form->field($model, 'titulo_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\Titulo::find()->orderBy('id')->asArray()->all(), 'id', 'id'),
        'options' => ['placeholder' => 'Choose Titulo'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); */ ?>

    <?php /* echo $form->field($model, 'estado_civil_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\EstadoCivil::find()->orderBy('id')->asArray()->all(), 'id', 'id'),
        'options' => ['placeholder' => 'Choose Estado civil'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); */ ?>

    <?php /* echo $form->field($model, 'sexo_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\Sexo::find()->orderBy('id')->asArray()->all(), 'id', 'id'),
        'options' => ['placeholder' => 'Choose Sexo'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); */ ?>

    <?php /* echo $form->field($model, 'usuario_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\Usuario::find()->orderBy('id')->asArray()->all(), 'id', 'id'),
        'options' => ['placeholder' => 'Choose Usuario'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); */ ?>

    <?php /* echo $form->field($model, 'endereco_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\Endereco::find()->orderBy('id')->asArray()->all(), 'id', 'id'),
        'options' => ['placeholder' => 'Choose Endereco'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); */ ?>

    <?php /* echo $form->field($model, 'tipo_servico_publico_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\TipoServicoPublico::find()->orderBy('id')->asArray()->all(), 'id', 'id'),
        'options' => ['placeholder' => 'Choose Tipo servico publico'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); */ ?>

    <?php /* echo $form->field($model, 'vinculo_servico_publico')->checkbox() */ ?>

    <?php /* echo $form->field($model, 'tipo_dedicacao_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\TipoDedicacao::find()->orderBy('id')->asArray()->all(), 'id', 'id'),
        'options' => ['placeholder' => 'Choose Tipo dedicacao'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); */ ?>

    <?php /* echo $form->field($model, 'nome_orgao_publico')->textInput(['maxlength' => true, 'placeholder' => 'Nome Orgao Publico']) */ ?>

    <?php /* echo $form->field($model, 'situacao_contratual_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\SituacaoContratual::find()->orderBy('id')->asArray()->all(), 'id', 'id'),
        'options' => ['placeholder' => 'Choose Situacao contratual'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); */ ?>

    <?php /* echo $form->field($model, 'familiar_onu_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\FamiliarOnu::find()->orderBy('id')->asArray()->all(), 'id', 'id'),
        'options' => ['placeholder' => 'Choose Familiar onu'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); */ ?>

    <?php /* echo $form->field($model, 'id_econtracting_antigo')->textInput(['placeholder' => 'Id Econtracting Antigo']) */ ?>

    <?php /* echo $form->field($model, 'trabalha_atualmente_onu')->checkbox() */ ?>

    <?php /* echo $form->field($model, 'trabalha_atualmente_opas')->checkbox() */ ?>

    <?php /* echo $form->field($model, 'qualificacao_info')->textarea(['rows' => 6]) */ ?>

    <?php /* echo $form->field($model, 'publicacao')->textarea(['rows' => 6]) */ ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
