<?php
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;

    $dataProvider = new ArrayDataProvider([
        'allModels' => $model->pessoaEscolaridades,
        'key' => 'id'
    ]);
    $gridColumns = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        'curso',
        'nome_instituicao',
        'dt_inicio',
        'dt_fim',
        [
                'attribute' => 'situacaoCurso.id',
                'label' => 'Situacao Curso'
            ],
        [
                'attribute' => 'escolaridade.id',
                'label' => 'Escolaridade'
            ],
        'estado',
        'cidade',
        [
                'attribute' => 'pais.id',
                'label' => 'Pais'
            ],
        [
                'attribute' => 'cursoFormacao.id',
                'label' => 'Curso Formacao'
            ],
        [
            'class' => 'yii\grid\ActionColumn',
            'controller' => 'pessoa-escolaridade'
        ],
    ];
    
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'containerOptions' => ['style' => 'overflow: auto'],
        'pjax' => true,
        'beforeHeader' => [
            [
                'options' => ['class' => 'skip-export']
            ]
        ],
        'export' => [
            'fontAwesome' => true
        ],
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'showPageSummary' => false,
        'persistResize' => false,
    ]);
