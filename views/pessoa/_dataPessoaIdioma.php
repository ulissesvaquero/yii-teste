<?php
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;

    $dataProvider = new ArrayDataProvider([
        'allModels' => $model->pessoaIdiomas,
        'key' => 'id'
    ]);
    $gridColumns = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        [
                'attribute' => 'nivelOral.id',
                'label' => 'Nivel Oral'
            ],
        [
                'attribute' => 'nivelLeitura.id',
                'label' => 'Nivel Leitura'
            ],
        [
                'attribute' => 'nivelEscrita.id',
                'label' => 'Nivel Escrita'
            ],
        [
                'attribute' => 'idioma.id',
                'label' => 'Idioma'
            ],
        [
                'attribute' => 'infoAdicionalIdioma.id',
                'label' => 'Info Adicional Idioma'
            ],
        [
            'class' => 'yii\grid\ActionColumn',
            'controller' => 'pessoa-idioma'
        ],
    ];
    
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'containerOptions' => ['style' => 'overflow: auto'],
        'pjax' => true,
        'beforeHeader' => [
            [
                'options' => ['class' => 'skip-export']
            ]
        ],
        'export' => [
            'fontAwesome' => true
        ],
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'showPageSummary' => false,
        'persistResize' => false,
    ]);
