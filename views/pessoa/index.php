<?php

/* @var $this yii\web\View */
/* @var $searchModel app\models\PessoaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use yii\helpers\Html;
use kartik\export\ExportMenu;
use kartik\grid\GridView;

$this->title = 'Pessoas';
$this->params['breadcrumbs'][] = $this->title;
$search = "$('.search-button').click(function(){
	$('.search-form').toggle(1000);
	return false;
});";
$this->registerJs($search);
?>
<div class="pessoa-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Pessoa', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Advance Search', '#', ['class' => 'btn btn-info search-button']) ?>
    </p>
    <div class="search-form" style="display:none">
        <?=  $this->render('_search', ['model' => $searchModel]); ?>
    </div>
    <?php 
    $gridColumn = [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'class' => 'kartik\grid\ExpandRowColumn',
            'width' => '50px',
            'value' => function ($model, $key, $index, $column) {
                return GridView::ROW_COLLAPSED;
            },
            'detail' => function ($model, $key, $index, $column) {
                return Yii::$app->controller->renderPartial('_expand', ['model' => $model]);
            },
            'headerOptions' => ['class' => 'kartik-sheet-style'],
            'expandOneOnly' => true
        ],
        ['attribute' => 'id', 'visible' => false],
        'nome',
        'sobrenome',
        'dt_nascimento',
        'telefone_1',
        'telefone_2',
        'email:email',
        'url_blog:url',
        'url_linkedin:url',
        'url_site:url',
        'twitter',
        'url_facebook:url',
        'skype',
        'url_foto:url',
        [
                'attribute' => 'titulo_id',
                'label' => 'Titulo',
                'value' => function($model){
                    return $model->titulo->id;
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\app\models\Titulo::find()->asArray()->all(), 'id', 'id'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Titulo', 'id' => 'grid-pessoa-search-titulo_id']
            ],
        [
                'attribute' => 'estado_civil_id',
                'label' => 'Estado Civil',
                'value' => function($model){
                    return $model->estadoCivil->id;
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\app\models\EstadoCivil::find()->asArray()->all(), 'id', 'id'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Estado civil', 'id' => 'grid-pessoa-search-estado_civil_id']
            ],
        [
                'attribute' => 'sexo_id',
                'label' => 'Sexo',
                'value' => function($model){
                    return $model->sexo->id;
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\app\models\Sexo::find()->asArray()->all(), 'id', 'id'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Sexo', 'id' => 'grid-pessoa-search-sexo_id']
            ],
        [
                'attribute' => 'usuario_id',
                'label' => 'Usuario',
                'value' => function($model){
                    return $model->usuario->id;
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\app\models\Usuario::find()->asArray()->all(), 'id', 'id'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Usuario', 'id' => 'grid-pessoa-search-usuario_id']
            ],
        [
                'attribute' => 'endereco_id',
                'label' => 'Endereco',
                'value' => function($model){
                    //return $model->endereco->id;
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\app\models\Endereco::find()->asArray()->all(), 'id', 'id'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Endereco', 'id' => 'grid-pessoa-search-endereco_id']
            ],
        [
                'attribute' => 'tipo_servico_publico_id',
                'label' => 'Tipo Servico Publico',
                'value' => function($model){
                   // return $model->tipoServicoPublico->id;
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\app\models\TipoServicoPublico::find()->asArray()->all(), 'id', 'id'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Tipo servico publico', 'id' => 'grid-pessoa-search-tipo_servico_publico_id']
            ],
        'vinculo_servico_publico',
        [
                'attribute' => 'tipo_dedicacao_id',
                'label' => 'Tipo Dedicacao',
                'value' => function($model){
                   // return $model->tipoDedicacao->id;
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\app\models\TipoDedicacao::find()->asArray()->all(), 'id', 'id'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Tipo dedicacao', 'id' => 'grid-pessoa-search-tipo_dedicacao_id']
            ],
        'nome_orgao_publico',
        [
                'attribute' => 'situacao_contratual_id',
                'label' => 'Situacao Contratual',
                'value' => function($model){
                    //return $model->situacaoContratual->id;
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\app\models\SituacaoContratual::find()->asArray()->all(), 'id', 'id'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Situacao contratual', 'id' => 'grid-pessoa-search-situacao_contratual_id']
            ],
        [
                'attribute' => 'familiar_onu_id',
                'label' => 'Familiar Onu',
                'value' => function($model){
                    //return $model->familiarOnu->id;
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\app\models\FamiliarOnu::find()->asArray()->all(), 'id', 'id'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Familiar onu', 'id' => 'grid-pessoa-search-familiar_onu_id']
            ],
        'id_econtracting_antigo',
        'trabalha_atualmente_onu',
        'trabalha_atualmente_opas',
        //	'qualificacao_info:ntext',
       // 'publicacao:ntext',
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{save-as-new} {view} {update} {delete}',
            'buttons' => [
                'save-as-new' => function ($url) {
                    return Html::a('<span class="glyphicon glyphicon-copy"></span>', $url, ['title' => 'Save As New']);
                },
            ],
        ],
    ]; 
    ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumn,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-pessoa']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span>  ' . Html::encode($this->title),
        ],
        // your toolbar can include the additional full export menu
        'toolbar' => [
            '{export}',
            ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => $gridColumn,
                'target' => ExportMenu::TARGET_BLANK,
                'fontAwesome' => true,
                'dropdownOptions' => [
                    'label' => 'Full',
                    'class' => 'btn btn-default',
                    'itemsBefore' => [
                        '<li class="dropdown-header">Export All Data</li>',
                    ],
                ],
            ]) ,
        ],
    ]); ?>

</div>
