<?php
use yii\helpers\Html;
use kartik\tabs\TabsX;
use yii\helpers\Url;
$items = [
    [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Pessoa'),
        'content' => $this->render('_detail', [
            'model' => $model,
        ]),
    ],
        [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Acesso Pessoa Menu'),
        'content' => $this->render('_dataAcessoPessoaMenu', [
            'model' => $model,
            'row' => $model->acessoPessoaMenus,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Avaliador'),
        'content' => $this->render('_dataAvaliador', [
            'model' => $model,
            'row' => $model->avaliadors,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Experiencia Profissional'),
        'content' => $this->render('_dataExperienciaProfissional', [
            'model' => $model,
            'row' => $model->experienciaProfissionals,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Experiencia Profissional Internacional'),
        'content' => $this->render('_dataExperienciaProfissionalInternacional', [
            'model' => $model,
            'row' => $model->experienciaProfissionalInternacionals,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Habilidade Informatica'),
        'content' => $this->render('_dataHabilidadeInformatica', [
            'model' => $model,
            'row' => $model->habilidadeInformaticas,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Oportunidade Aplicada'),
        'content' => $this->render('_dataOportunidadeAplicada', [
            'model' => $model,
            'row' => $model->oportunidadeAplicadas,
        ]),
    ],
                                                [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Pessoa Area Atuacao'),
        'content' => $this->render('_dataPessoaAreaAtuacao', [
            'model' => $model,
            'row' => $model->pessoaAreaAtuacaos,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Pessoa Escolaridade'),
        'content' => $this->render('_dataPessoaEscolaridade', [
            'model' => $model,
            'row' => $model->pessoaEscolaridades,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Pessoa Idioma'),
        'content' => $this->render('_dataPessoaIdioma', [
            'model' => $model,
            'row' => $model->pessoaIdiomas,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Pessoa Nacionalidade'),
        'content' => $this->render('_dataPessoaNacionalidade', [
            'model' => $model,
            'row' => $model->pessoaNacionalidades,
        ]),
    ],
                [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Pessoa Storage'),
        'content' => $this->render('_dataPessoaStorage', [
            'model' => $model,
            'row' => $model->pessoaStorages,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Pessoa Tipo Contrato'),
        'content' => $this->render('_dataPessoaTipoContrato', [
            'model' => $model,
            'row' => $model->pessoaTipoContratos,
        ]),
    ],
                [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Referencia'),
        'content' => $this->render('_dataReferencia', [
            'model' => $model,
            'row' => $model->referencias,
        ]),
    ],
    ];
echo TabsX::widget([
    'items' => $items,
    'position' => TabsX::POS_ABOVE,
    'encodeLabels' => false,
    'class' => 'tes',
    'pluginOptions' => [
        'bordered' => true,
        'sideways' => true,
        'enableCache' => false
    ],
]);
?>
