<?php

namespace app\controllers;

use Yii;
use app\models\Pessoa;
use app\models\PessoaSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PessoaController implements the CRUD actions for Pessoa model.
 */
class PessoaController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view', 'create', 'update', 'delete', 'pdf', 'save-as-new', 'add-acesso-pessoa-menu', 'add-avaliador', 'add-experiencia-profissional', 'add-experiencia-profissional-internacional', 'add-habilidade-informatica', 'add-oportunidade-aplicada', 'add-pessoa-area-atuacao', 'add-pessoa-escolaridade', 'add-pessoa-idioma', 'add-pessoa-nacionalidade', 'add-pessoa-storage', 'add-pessoa-tipo-contrato', 'add-referencia'],
                        'roles' => ['@']
                    ],
                    [
                        'allow' => false
                    ]
                ]
            ]
        ];
    }

    /**
     * Lists all Pessoa models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PessoaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		
        $dataProvider->pagination = ['pageSize' => 2];
        
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Pessoa model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $providerAcessoPessoaMenu = new \yii\data\ArrayDataProvider([
            'allModels' => $model->acessoPessoaMenus,
        ]);
        $providerAvaliador = new \yii\data\ArrayDataProvider([
            'allModels' => $model->avaliadors,
        ]);
        $providerExperienciaProfissional = new \yii\data\ArrayDataProvider([
            'allModels' => $model->experienciaProfissionals,
        ]);
        $providerExperienciaProfissionalInternacional = new \yii\data\ArrayDataProvider([
            'allModels' => $model->experienciaProfissionalInternacionals,
        ]);
        $providerHabilidadeInformatica = new \yii\data\ArrayDataProvider([
            'allModels' => $model->habilidadeInformaticas,
        ]);
        $providerOportunidadeAplicada = new \yii\data\ArrayDataProvider([
            'allModels' => $model->oportunidadeAplicadas,
        ]);
        $providerPessoaAreaAtuacao = new \yii\data\ArrayDataProvider([
            'allModels' => $model->pessoaAreaAtuacaos,
        ]);
        $providerPessoaEscolaridade = new \yii\data\ArrayDataProvider([
            'allModels' => $model->pessoaEscolaridades,
        ]);
        $providerPessoaIdioma = new \yii\data\ArrayDataProvider([
            'allModels' => $model->pessoaIdiomas,
        ]);
        $providerPessoaNacionalidade = new \yii\data\ArrayDataProvider([
            'allModels' => $model->pessoaNacionalidades,
        ]);
        $providerPessoaStorage = new \yii\data\ArrayDataProvider([
            'allModels' => $model->pessoaStorages,
        ]);
        $providerPessoaTipoContrato = new \yii\data\ArrayDataProvider([
            'allModels' => $model->pessoaTipoContratos,
        ]);
        $providerReferencia = new \yii\data\ArrayDataProvider([
            'allModels' => $model->referencias,
        ]);
        return $this->render('view', [
            'model' => $this->findModel($id),
            'providerAcessoPessoaMenu' => $providerAcessoPessoaMenu,
            'providerAvaliador' => $providerAvaliador,
            'providerExperienciaProfissional' => $providerExperienciaProfissional,
            'providerExperienciaProfissionalInternacional' => $providerExperienciaProfissionalInternacional,
            'providerHabilidadeInformatica' => $providerHabilidadeInformatica,
            'providerOportunidadeAplicada' => $providerOportunidadeAplicada,
            'providerPessoaAreaAtuacao' => $providerPessoaAreaAtuacao,
            'providerPessoaEscolaridade' => $providerPessoaEscolaridade,
            'providerPessoaIdioma' => $providerPessoaIdioma,
            'providerPessoaNacionalidade' => $providerPessoaNacionalidade,
            'providerPessoaStorage' => $providerPessoaStorage,
            'providerPessoaTipoContrato' => $providerPessoaTipoContrato,
            'providerReferencia' => $providerReferencia,
        ]);
    }

    /**
     * Creates a new Pessoa model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Pessoa();

        if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Pessoa model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if (Yii::$app->request->post('_asnew') == '1') {
            $model = new Pessoa();
        }else{
            $model = $this->findModel($id);
        }

        if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Pessoa model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->deleteWithRelated();

        return $this->redirect(['index']);
    }
    
    /**
     * 
     * Export Pessoa information into PDF format.
     * @param integer $id
     * @return mixed
     */
    public function actionPdf($id) {
        $model = $this->findModel($id);
        $providerAcessoPessoaMenu = new \yii\data\ArrayDataProvider([
            'allModels' => $model->acessoPessoaMenus,
        ]);
        $providerAvaliador = new \yii\data\ArrayDataProvider([
            'allModels' => $model->avaliadors,
        ]);
        $providerExperienciaProfissional = new \yii\data\ArrayDataProvider([
            'allModels' => $model->experienciaProfissionals,
        ]);
        $providerExperienciaProfissionalInternacional = new \yii\data\ArrayDataProvider([
            'allModels' => $model->experienciaProfissionalInternacionals,
        ]);
        $providerHabilidadeInformatica = new \yii\data\ArrayDataProvider([
            'allModels' => $model->habilidadeInformaticas,
        ]);
        $providerOportunidadeAplicada = new \yii\data\ArrayDataProvider([
            'allModels' => $model->oportunidadeAplicadas,
        ]);
        $providerPessoaAreaAtuacao = new \yii\data\ArrayDataProvider([
            'allModels' => $model->pessoaAreaAtuacaos,
        ]);
        $providerPessoaEscolaridade = new \yii\data\ArrayDataProvider([
            'allModels' => $model->pessoaEscolaridades,
        ]);
        $providerPessoaIdioma = new \yii\data\ArrayDataProvider([
            'allModels' => $model->pessoaIdiomas,
        ]);
        $providerPessoaNacionalidade = new \yii\data\ArrayDataProvider([
            'allModels' => $model->pessoaNacionalidades,
        ]);
        $providerPessoaStorage = new \yii\data\ArrayDataProvider([
            'allModels' => $model->pessoaStorages,
        ]);
        $providerPessoaTipoContrato = new \yii\data\ArrayDataProvider([
            'allModels' => $model->pessoaTipoContratos,
        ]);
        $providerReferencia = new \yii\data\ArrayDataProvider([
            'allModels' => $model->referencias,
        ]);

        $content = $this->renderAjax('_pdf', [
            'model' => $model,
            'providerAcessoPessoaMenu' => $providerAcessoPessoaMenu,
            'providerAvaliador' => $providerAvaliador,
            'providerExperienciaProfissional' => $providerExperienciaProfissional,
            'providerExperienciaProfissionalInternacional' => $providerExperienciaProfissionalInternacional,
            'providerHabilidadeInformatica' => $providerHabilidadeInformatica,
            'providerOportunidadeAplicada' => $providerOportunidadeAplicada,
            'providerPessoaAreaAtuacao' => $providerPessoaAreaAtuacao,
            'providerPessoaEscolaridade' => $providerPessoaEscolaridade,
            'providerPessoaIdioma' => $providerPessoaIdioma,
            'providerPessoaNacionalidade' => $providerPessoaNacionalidade,
            'providerPessoaStorage' => $providerPessoaStorage,
            'providerPessoaTipoContrato' => $providerPessoaTipoContrato,
            'providerReferencia' => $providerReferencia,
        ]);

        $pdf = new \kartik\mpdf\Pdf([
            'mode' => \kartik\mpdf\Pdf::MODE_CORE,
            'format' => \kartik\mpdf\Pdf::FORMAT_A4,
            'orientation' => \kartik\mpdf\Pdf::ORIENT_PORTRAIT,
            'destination' => \kartik\mpdf\Pdf::DEST_BROWSER,
            'content' => $content,
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
            'cssInline' => '.kv-heading-1{font-size:18px}',
            'options' => ['title' => \Yii::$app->name],
            'methods' => [
                'SetHeader' => [\Yii::$app->name],
                'SetFooter' => ['{PAGENO}'],
            ]
        ]);

        return $pdf->render();
    }

    /**
    * Creates a new Pessoa model by another data,
    * so user don't need to input all field from scratch.
    * If creation is successful, the browser will be redirected to the 'view' page.
    *
    * @param type $id
    * @return type
    */
    public function actionSaveAsNew($id) {
        $model = new Pessoa();

        if (Yii::$app->request->post('_asnew') != '1') {
            $model = $this->findModel($id);
        }
    
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('saveAsNew', [
                'model' => $model,
            ]);
        }
    }
    
    /**
     * Finds the Pessoa model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Pessoa the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Pessoa::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    /**
    * Action to load a tabular form grid
    * for AcessoPessoaMenu
    * @author Yohanes Candrajaya <moo.tensai@gmail.com>
    * @author Jiwantoro Ndaru <jiwanndaru@gmail.com>
    *
    * @return mixed
    */
    public function actionAddAcessoPessoaMenu()
    {
        if (Yii::$app->request->isAjax) {
            $row = Yii::$app->request->post('AcessoPessoaMenu');
            if((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add')
                $row[] = [];
            return $this->renderAjax('_formAcessoPessoaMenu', ['row' => $row]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    /**
    * Action to load a tabular form grid
    * for Avaliador
    * @author Yohanes Candrajaya <moo.tensai@gmail.com>
    * @author Jiwantoro Ndaru <jiwanndaru@gmail.com>
    *
    * @return mixed
    */
    public function actionAddAvaliador()
    {
        if (Yii::$app->request->isAjax) {
            $row = Yii::$app->request->post('Avaliador');
            if((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add')
                $row[] = [];
            return $this->renderAjax('_formAvaliador', ['row' => $row]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    /**
    * Action to load a tabular form grid
    * for ExperienciaProfissional
    * @author Yohanes Candrajaya <moo.tensai@gmail.com>
    * @author Jiwantoro Ndaru <jiwanndaru@gmail.com>
    *
    * @return mixed
    */
    public function actionAddExperienciaProfissional()
    {
        if (Yii::$app->request->isAjax) {
            $row = Yii::$app->request->post('ExperienciaProfissional');
            if((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add')
                $row[] = [];
            return $this->renderAjax('_formExperienciaProfissional', ['row' => $row]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    /**
    * Action to load a tabular form grid
    * for ExperienciaProfissionalInternacional
    * @author Yohanes Candrajaya <moo.tensai@gmail.com>
    * @author Jiwantoro Ndaru <jiwanndaru@gmail.com>
    *
    * @return mixed
    */
    public function actionAddExperienciaProfissionalInternacional()
    {
        if (Yii::$app->request->isAjax) {
            $row = Yii::$app->request->post('ExperienciaProfissionalInternacional');
            if((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add')
                $row[] = [];
            return $this->renderAjax('_formExperienciaProfissionalInternacional', ['row' => $row]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    /**
    * Action to load a tabular form grid
    * for HabilidadeInformatica
    * @author Yohanes Candrajaya <moo.tensai@gmail.com>
    * @author Jiwantoro Ndaru <jiwanndaru@gmail.com>
    *
    * @return mixed
    */
    public function actionAddHabilidadeInformatica()
    {
        if (Yii::$app->request->isAjax) {
            $row = Yii::$app->request->post('HabilidadeInformatica');
            if((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add')
                $row[] = [];
            return $this->renderAjax('_formHabilidadeInformatica', ['row' => $row]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    /**
    * Action to load a tabular form grid
    * for OportunidadeAplicada
    * @author Yohanes Candrajaya <moo.tensai@gmail.com>
    * @author Jiwantoro Ndaru <jiwanndaru@gmail.com>
    *
    * @return mixed
    */
    public function actionAddOportunidadeAplicada()
    {
        if (Yii::$app->request->isAjax) {
            $row = Yii::$app->request->post('OportunidadeAplicada');
            if((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add')
                $row[] = [];
            return $this->renderAjax('_formOportunidadeAplicada', ['row' => $row]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    /**
    * Action to load a tabular form grid
    * for PessoaAreaAtuacao
    * @author Yohanes Candrajaya <moo.tensai@gmail.com>
    * @author Jiwantoro Ndaru <jiwanndaru@gmail.com>
    *
    * @return mixed
    */
    public function actionAddPessoaAreaAtuacao()
    {
        if (Yii::$app->request->isAjax) {
            $row = Yii::$app->request->post('PessoaAreaAtuacao');
            if((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add')
                $row[] = [];
            return $this->renderAjax('_formPessoaAreaAtuacao', ['row' => $row]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    /**
    * Action to load a tabular form grid
    * for PessoaEscolaridade
    * @author Yohanes Candrajaya <moo.tensai@gmail.com>
    * @author Jiwantoro Ndaru <jiwanndaru@gmail.com>
    *
    * @return mixed
    */
    public function actionAddPessoaEscolaridade()
    {
        if (Yii::$app->request->isAjax) {
            $row = Yii::$app->request->post('PessoaEscolaridade');
            if((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add')
                $row[] = [];
            return $this->renderAjax('_formPessoaEscolaridade', ['row' => $row]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    /**
    * Action to load a tabular form grid
    * for PessoaIdioma
    * @author Yohanes Candrajaya <moo.tensai@gmail.com>
    * @author Jiwantoro Ndaru <jiwanndaru@gmail.com>
    *
    * @return mixed
    */
    public function actionAddPessoaIdioma()
    {
        if (Yii::$app->request->isAjax) {
            $row = Yii::$app->request->post('PessoaIdioma');
            if((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add')
                $row[] = [];
            return $this->renderAjax('_formPessoaIdioma', ['row' => $row]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    /**
    * Action to load a tabular form grid
    * for PessoaNacionalidade
    * @author Yohanes Candrajaya <moo.tensai@gmail.com>
    * @author Jiwantoro Ndaru <jiwanndaru@gmail.com>
    *
    * @return mixed
    */
    public function actionAddPessoaNacionalidade()
    {
        if (Yii::$app->request->isAjax) {
            $row = Yii::$app->request->post('PessoaNacionalidade');
            if((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add')
                $row[] = [];
            return $this->renderAjax('_formPessoaNacionalidade', ['row' => $row]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    /**
    * Action to load a tabular form grid
    * for PessoaStorage
    * @author Yohanes Candrajaya <moo.tensai@gmail.com>
    * @author Jiwantoro Ndaru <jiwanndaru@gmail.com>
    *
    * @return mixed
    */
    public function actionAddPessoaStorage()
    {
        if (Yii::$app->request->isAjax) {
            $row = Yii::$app->request->post('PessoaStorage');
            if((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add')
                $row[] = [];
            return $this->renderAjax('_formPessoaStorage', ['row' => $row]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    /**
    * Action to load a tabular form grid
    * for PessoaTipoContrato
    * @author Yohanes Candrajaya <moo.tensai@gmail.com>
    * @author Jiwantoro Ndaru <jiwanndaru@gmail.com>
    *
    * @return mixed
    */
    public function actionAddPessoaTipoContrato()
    {
        if (Yii::$app->request->isAjax) {
            $row = Yii::$app->request->post('PessoaTipoContrato');
            if((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add')
                $row[] = [];
            return $this->renderAjax('_formPessoaTipoContrato', ['row' => $row]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    /**
    * Action to load a tabular form grid
    * for Referencia
    * @author Yohanes Candrajaya <moo.tensai@gmail.com>
    * @author Jiwantoro Ndaru <jiwanndaru@gmail.com>
    *
    * @return mixed
    */
    public function actionAddReferencia()
    {
        if (Yii::$app->request->isAjax) {
            $row = Yii::$app->request->post('Referencia');
            if((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add')
                $row[] = [];
            return $this->renderAjax('_formReferencia', ['row' => $row]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
