<?php

namespace app\models;

use \app\models\base\OportunidadeEscalaCompetenciaComportamental as BaseOportunidadeEscalaCompetenciaComportamental;

/**
 * This is the model class for table "oportunidade_escala_competencia_comportamental".
 */
class OportunidadeEscalaCompetenciaComportamental extends BaseOportunidadeEscalaCompetenciaComportamental
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['oportunidade_id', 'escala_competencia_comportamental_id'], 'required'],
            [['oportunidade_id', 'escala_competencia_comportamental_id'], 'integer'],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
}
