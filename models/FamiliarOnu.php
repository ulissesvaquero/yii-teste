<?php

namespace app\models;

use \app\models\base\FamiliarOnu as BaseFamiliarOnu;

/**
 * This is the model class for table "familiar_onu".
 */
class FamiliarOnu extends BaseFamiliarOnu
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['tipo_contrato_internacional_id'], 'required'],
            [['tipo_contrato_internacional_id'], 'integer'],
            [['nome_parente', 'agencia'], 'string', 'max' => 200],
            [['parentesco'], 'string', 'max' => 100],
            [['md5_nome_parente'], 'string', 'max' => 150],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
}
