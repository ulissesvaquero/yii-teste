<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[AnoExperiencia]].
 *
 * @see AnoExperiencia
 */
class AnoExperienciaQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return AnoExperiencia[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return AnoExperiencia|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}