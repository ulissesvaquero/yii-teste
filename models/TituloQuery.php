<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Titulo]].
 *
 * @see Titulo
 */
class TituloQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return Titulo[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Titulo|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}