<?php

namespace app\models;

use \app\models\base\FerramentaInformatica as BaseFerramentaInformatica;

/**
 * This is the model class for table "ferramenta_informatica".
 */
class FerramentaInformatica extends BaseFerramentaInformatica
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['nome'], 'string', 'max' => 100],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
}
