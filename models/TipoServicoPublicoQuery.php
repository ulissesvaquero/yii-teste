<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[TipoServicoPublico]].
 *
 * @see TipoServicoPublico
 */
class TipoServicoPublicoQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return TipoServicoPublico[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return TipoServicoPublico|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}