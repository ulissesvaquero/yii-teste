<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Uf]].
 *
 * @see Uf
 */
class UfQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return Uf[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Uf|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}