<?php

namespace app\models;

use \app\models\base\Produto as BaseProduto;

/**
 * This is the model class for table "produto".
 */
class Produto extends BaseProduto
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['numero', 'edital_id'], 'integer'],
            [['descricao'], 'string'],
            [['data'], 'safe'],
            [['valor'], 'number'],
            [['edital_id'], 'required'],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
}
