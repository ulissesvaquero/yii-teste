<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[PessoaTipoContrato]].
 *
 * @see PessoaTipoContrato
 */
class PessoaTipoContratoQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return PessoaTipoContrato[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return PessoaTipoContrato|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}