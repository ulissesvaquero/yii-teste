<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[TipoContratoInternacional]].
 *
 * @see TipoContratoInternacional
 */
class TipoContratoInternacionalQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return TipoContratoInternacional[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return TipoContratoInternacional|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}