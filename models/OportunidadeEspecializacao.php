<?php

namespace app\models;

use \app\models\base\OportunidadeEspecializacao as BaseOportunidadeEspecializacao;

/**
 * This is the model class for table "oportunidade_especializacao".
 */
class OportunidadeEspecializacao extends BaseOportunidadeEspecializacao
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['oportunidade_id', 'especializacao_id'], 'required'],
            [['oportunidade_id', 'especializacao_id'], 'integer'],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
}
