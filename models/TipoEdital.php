<?php

namespace app\models;

use \app\models\base\TipoEdital as BaseTipoEdital;

/**
 * This is the model class for table "tipo_edital".
 */
class TipoEdital extends BaseTipoEdital
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['nome'], 'string', 'max' => 200],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
}
