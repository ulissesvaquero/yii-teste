<?php

namespace app\models;

use \app\models\base\RespostaOportunidadeAplicada as BaseRespostaOportunidadeAplicada;

/**
 * This is the model class for table "resposta_oportunidade_aplicada".
 */
class RespostaOportunidadeAplicada extends BaseRespostaOportunidadeAplicada
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['oportunidade_aplicada_id', 'questao_id'], 'required'],
            [['oportunidade_aplicada_id', 'is_correta', 'questao_id'], 'integer'],
            [['resposta'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
}
