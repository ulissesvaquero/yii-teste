<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[SituacaoContratual]].
 *
 * @see SituacaoContratual
 */
class SituacaoContratualQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return SituacaoContratual[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return SituacaoContratual|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}