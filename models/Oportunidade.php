<?php

namespace app\models;

use \app\models\base\Oportunidade as BaseOportunidade;

/**
 * This is the model class for table "oportunidade".
 */
class Oportunidade extends BaseOportunidade
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['edital_id', 'vaga_id', 'pais_id', 'cidade_id', 'ut_id', 'uf_id', 'fonte_financiamento_id', 'status_oportunidade_id'], 'integer'],
            [['data_inicio_divulgacao', 'data_fim_divulgacao', 'data_publicacao'], 'safe'],
            [['pais_id', 'fonte_financiamento_id', 'status_oportunidade_id'], 'required'],
            [['competencia_tecnica'], 'string'],
            [['titulo', 'nome_estado', 'nome_cidade', 'oficial_responsavel'], 'string', 'max' => 200],
            [['tipo_oportunidade'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
}
