<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Avaliador]].
 *
 * @see Avaliador
 */
class AvaliadorQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return Avaliador[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Avaliador|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}