<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[TipoFinanciamentoVaga]].
 *
 * @see TipoFinanciamentoVaga
 */
class TipoFinanciamentoVagaQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return TipoFinanciamentoVaga[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return TipoFinanciamentoVaga|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}