<?php

namespace app\models;

use \app\models\base\TipoMoeda as BaseTipoMoeda;

/**
 * This is the model class for table "tipo_moeda".
 */
class TipoMoeda extends BaseTipoMoeda
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['nome'], 'string', 'max' => 45],
            [['sigla'], 'string', 'max' => 2],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
}
