<?php

namespace app\models;

use \app\models\base\OportunidadeAplicada as BaseOportunidadeAplicada;

/**
 * This is the model class for table "oportunidade_aplicada".
 */
class OportunidadeAplicada extends BaseOportunidadeAplicada
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['data_aplicacacao'], 'safe'],
            [['pessoa_id', 'oportunidade_id'], 'required'],
            [['pessoa_id', 'oportunidade_id', 'is_aprovado'], 'integer'],
            [['score'], 'number'],
            [['carta_apresentacao'], 'string'],
            [['assinatura'], 'string', 'max' => 500],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
}
