<?php

namespace app\models;

use \app\models\base\AnoExperiencia as BaseAnoExperiencia;

/**
 * This is the model class for table "ano_experiencia".
 */
class AnoExperiencia extends BaseAnoExperiencia
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['nome'], 'string', 'max' => 100],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
}
