<?php

namespace app\models;

use \app\models\base\EstadoCivil as BaseEstadoCivil;

/**
 * This is the model class for table "estado_civil".
 */
class EstadoCivil extends BaseEstadoCivil
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['nome'], 'string', 'max' => 100],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
}
