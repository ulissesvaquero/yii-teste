<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[RespostaQuestao]].
 *
 * @see RespostaQuestao
 */
class RespostaQuestaoQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return RespostaQuestao[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return RespostaQuestao|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}