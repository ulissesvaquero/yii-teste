<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[CompetenciaComportamental]].
 *
 * @see CompetenciaComportamental
 */
class CompetenciaComportamentalQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return CompetenciaComportamental[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return CompetenciaComportamental|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}