<?php

namespace app\models;

use \app\models\base\Avaliador as BaseAvaliador;

/**
 * This is the model class for table "avaliador".
 */
class Avaliador extends BaseAvaliador
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['pessoa_id', 'tipo_avaliador_id', 'oportunidade_id'], 'required'],
            [['pessoa_id', 'tipo_avaliador_id', 'oportunidade_id'], 'integer'],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
}
