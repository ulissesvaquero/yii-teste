<?php

namespace app\models;

use \app\models\base\TipoPublicacaoVaga as BaseTipoPublicacaoVaga;

/**
 * This is the model class for table "tipo_publicacao_vaga".
 */
class TipoPublicacaoVaga extends BaseTipoPublicacaoVaga
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['nome'], 'string', 'max' => 100],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
}
