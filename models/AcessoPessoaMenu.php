<?php

namespace app\models;

use \app\models\base\AcessoPessoaMenu as BaseAcessoPessoaMenu;

/**
 * This is the model class for table "acesso_pessoa_menu".
 */
class AcessoPessoaMenu extends BaseAcessoPessoaMenu
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['menu_id', 'pessoa_id'], 'required'],
            [['menu_id', 'pessoa_id', 'is_informado'], 'integer'],
            [['data_atualizacao'], 'safe'],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
}
