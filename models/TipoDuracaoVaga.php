<?php

namespace app\models;

use \app\models\base\TipoDuracaoVaga as BaseTipoDuracaoVaga;

/**
 * This is the model class for table "tipo_duracao_vaga".
 */
class TipoDuracaoVaga extends BaseTipoDuracaoVaga
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['nome'], 'string', 'max' => 100],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
}
