<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[TipoAvaliador]].
 *
 * @see TipoAvaliador
 */
class TipoAvaliadorQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return TipoAvaliador[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return TipoAvaliador|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}