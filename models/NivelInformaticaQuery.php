<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[NivelInformatica]].
 *
 * @see NivelInformatica
 */
class NivelInformaticaQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return NivelInformatica[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return NivelInformatica|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}