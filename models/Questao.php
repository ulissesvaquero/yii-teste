<?php

namespace app\models;

use \app\models\base\Questao as BaseQuestao;

/**
 * This is the model class for table "questao".
 */
class Questao extends BaseQuestao
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['descricao'], 'string'],
            [['tipo_questao_id', 'oportunidade_id'], 'required'],
            [['tipo_questao_id', 'oportunidade_id'], 'integer'],
            [['gabarito'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
}
