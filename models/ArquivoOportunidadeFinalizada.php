<?php

namespace app\models;

use \app\models\base\ArquivoOportunidadeFinalizada as BaseArquivoOportunidadeFinalizada;

/**
 * This is the model class for table "arquivo_oportunidade_finalizada".
 */
class ArquivoOportunidadeFinalizada extends BaseArquivoOportunidadeFinalizada
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['oportunidade_id'], 'required'],
            [['oportunidade_id'], 'integer'],
            [['caminho', 'nome'], 'string', 'max' => 500],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
}
