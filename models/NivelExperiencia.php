<?php

namespace app\models;

use \app\models\base\NivelExperiencia as BaseNivelExperiencia;

/**
 * This is the model class for table "nivel_experiencia".
 */
class NivelExperiencia extends BaseNivelExperiencia
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['nome'], 'string', 'max' => 100],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
}
