<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[PessoaStorage]].
 *
 * @see PessoaStorage
 */
class PessoaStorageQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return PessoaStorage[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return PessoaStorage|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}