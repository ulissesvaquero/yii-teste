<?php

namespace app\models;

use \app\models\base\Cidade as BaseCidade;

/**
 * This is the model class for table "cidade".
 */
class Cidade extends BaseCidade
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['uf_id'], 'integer'],
            [['nome'], 'string', 'max' => 200],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
}
