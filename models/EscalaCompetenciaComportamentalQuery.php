<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[EscalaCompetenciaComportamental]].
 *
 * @see EscalaCompetenciaComportamental
 */
class EscalaCompetenciaComportamentalQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return EscalaCompetenciaComportamental[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return EscalaCompetenciaComportamental|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}