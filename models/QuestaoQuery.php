<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Questao]].
 *
 * @see Questao
 */
class QuestaoQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return Questao[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Questao|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}