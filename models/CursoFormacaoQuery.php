<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[CursoFormacao]].
 *
 * @see CursoFormacao
 */
class CursoFormacaoQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return CursoFormacao[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return CursoFormacao|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}