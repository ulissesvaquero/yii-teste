<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[OportunidadeCursoFormacao]].
 *
 * @see OportunidadeCursoFormacao
 */
class OportunidadeCursoFormacaoQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return OportunidadeCursoFormacao[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return OportunidadeCursoFormacao|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}