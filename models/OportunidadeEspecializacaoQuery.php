<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[OportunidadeEspecializacao]].
 *
 * @see OportunidadeEspecializacao
 */
class OportunidadeEspecializacaoQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return OportunidadeEspecializacao[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return OportunidadeEspecializacao|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}