<?php

namespace app\models;

use \app\models\base\OportunidadeIdioma as BaseOportunidadeIdioma;

/**
 * This is the model class for table "oportunidade_idioma".
 */
class OportunidadeIdioma extends BaseOportunidadeIdioma
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['idioma_id', 'nivel_idioma_id', 'oportunidade_id'], 'required'],
            [['idioma_id', 'nivel_idioma_id', 'oportunidade_id'], 'integer'],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
}
