<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Escolaridade]].
 *
 * @see Escolaridade
 */
class EscolaridadeQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return Escolaridade[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Escolaridade|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}