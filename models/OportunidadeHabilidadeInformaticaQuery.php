<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[OportunidadeHabilidadeInformatica]].
 *
 * @see OportunidadeHabilidadeInformatica
 */
class OportunidadeHabilidadeInformaticaQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return OportunidadeHabilidadeInformatica[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return OportunidadeHabilidadeInformatica|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}