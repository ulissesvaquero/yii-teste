<?php

namespace app\models;

use \app\models\base\Idioma as BaseIdioma;

/**
 * This is the model class for table "idioma".
 */
class Idioma extends BaseIdioma
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['nome'], 'string', 'max' => 200],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
}
