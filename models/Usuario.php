<?php

namespace app\models;

use \app\models\base\Usuario as BaseUsuario;

/**
 * This is the model class for table "usuario".
 */
class Usuario extends BaseUsuario
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['ultimo_acesso'], 'safe'],
            [['status_cadastro'], 'integer'],
            [['email', 'senha'], 'string', 'max' => 100],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
}
