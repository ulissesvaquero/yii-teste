<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[AcessoPessoaMenu]].
 *
 * @see AcessoPessoaMenu
 */
class AcessoPessoaMenuQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return AcessoPessoaMenu[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return AcessoPessoaMenu|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}