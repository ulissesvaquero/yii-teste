<?php

namespace app\models;

use \app\models\base\TipoContratoInternacional as BaseTipoContratoInternacional;

/**
 * This is the model class for table "tipo_contrato_internacional".
 */
class TipoContratoInternacional extends BaseTipoContratoInternacional
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['nome'], 'string', 'max' => 100],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
}
