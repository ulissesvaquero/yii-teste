<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[TipoDuracaoVaga]].
 *
 * @see TipoDuracaoVaga
 */
class TipoDuracaoVagaQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return TipoDuracaoVaga[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return TipoDuracaoVaga|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}