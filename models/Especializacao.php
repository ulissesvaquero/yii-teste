<?php

namespace app\models;

use \app\models\base\Especializacao as BaseEspecializacao;

/**
 * This is the model class for table "especializacao".
 */
class Especializacao extends BaseEspecializacao
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['area_atuacao_id'], 'required'],
            [['area_atuacao_id', 'is_ativo', 'id_econtracting_antigo'], 'integer'],
            [['nome'], 'string', 'max' => 200],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
}
