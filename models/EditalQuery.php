<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Edital]].
 *
 * @see Edital
 */
class EditalQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return Edital[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Edital|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}