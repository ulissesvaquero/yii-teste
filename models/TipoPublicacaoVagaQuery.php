<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[TipoPublicacaoVaga]].
 *
 * @see TipoPublicacaoVaga
 */
class TipoPublicacaoVagaQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return TipoPublicacaoVaga[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return TipoPublicacaoVaga|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}