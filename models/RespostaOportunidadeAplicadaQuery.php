<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[RespostaOportunidadeAplicada]].
 *
 * @see RespostaOportunidadeAplicada
 */
class RespostaOportunidadeAplicadaQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return RespostaOportunidadeAplicada[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return RespostaOportunidadeAplicada|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}