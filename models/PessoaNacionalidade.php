<?php

namespace app\models;

use \app\models\base\PessoaNacionalidade as BasePessoaNacionalidade;

/**
 * This is the model class for table "pessoa_nacionalidade".
 */
class PessoaNacionalidade extends BasePessoaNacionalidade
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['pessoa_id', 'pais_id'], 'required'],
            [['pessoa_id', 'pais_id'], 'integer'],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
}
