<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Oportunidade]].
 *
 * @see Oportunidade
 */
class OportunidadeQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return Oportunidade[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Oportunidade|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}