<?php

namespace app\models;

use \app\models\base\PessoaStorage as BasePessoaStorage;

/**
 * This is the model class for table "pessoa_storage".
 */
class PessoaStorage extends BasePessoaStorage
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['pessoa_id', 'tipo_dado_storage_id'], 'required'],
            [['pessoa_id', 'tipo_dado_storage_id'], 'integer'],
            [['descricao'], 'string'],
            [['url'], 'string', 'max' => 500],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
}
