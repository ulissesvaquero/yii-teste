<?php

namespace app\models;

use \app\models\base\Vaga as BaseVaga;

/**
 * This is the model class for table "vaga".
 */
class Vaga extends BaseVaga
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['tipo_vaga_id', 'tipo_processo_vaga_id', 'tipo_duracao_vaga_id', 'qtd_mes', 'is_passivel_renovacao', 'tipo_contrato_id', 'tipo_moeda_salario_bruto_id'], 'integer'],
            [['tipo_processo_vaga_id', 'tipo_duracao_vaga_id', 'tipo_contrato_id', 'tipo_moeda_salario_bruto_id'], 'required'],
            [['salario_bruto'], 'number'],
            [['objetivo_geral', 'beneficio', 'exp_essencial', 'exp_desejavel', 'resumo_responsabilidade'], 'string'],
            [['numero_posto', 'numero_processo', 'fonte_financiamento'], 'string', 'max' => 100],
            [['lotacao', 'nome_outro_tipo_vaga'], 'string', 'max' => 45],
            [['subtarefa'], 'string', 'max' => 200],
            [['curso_formacao', 'nome_substituto_vaga'], 'string', 'max' => 300],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
}
