<?php

namespace app\models;

use \app\models\base\PessoaIdioma as BasePessoaIdioma;

/**
 * This is the model class for table "pessoa_idioma".
 */
class PessoaIdioma extends BasePessoaIdioma
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['pessoa_id', 'nivel_oral', 'nivel_leitura', 'nivel_escrita', 'idioma_id', 'info_adicional_idioma_id'], 'required'],
            [['pessoa_id', 'nivel_oral', 'nivel_leitura', 'nivel_escrita', 'idioma_id', 'info_adicional_idioma_id'], 'integer'],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
}
