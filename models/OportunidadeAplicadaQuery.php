<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[OportunidadeAplicada]].
 *
 * @see OportunidadeAplicada
 */
class OportunidadeAplicadaQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return OportunidadeAplicada[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return OportunidadeAplicada|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}