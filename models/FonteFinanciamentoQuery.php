<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[FonteFinanciamento]].
 *
 * @see FonteFinanciamento
 */
class FonteFinanciamentoQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return FonteFinanciamento[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return FonteFinanciamento|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}