<?php

namespace app\models;

use \app\models\base\Pessoa as BasePessoa;

/**
 * This is the model class for table "pessoa".
 */
class Pessoa extends BasePessoa
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['dt_nascimento'], 'safe'],
            [['titulo_id', 'estado_civil_id', 'sexo_id', 'usuario_id', 'endereco_id', 'tipo_servico_publico_id', 'vinculo_servico_publico', 'tipo_dedicacao_id', 'situacao_contratual_id', 'familiar_onu_id', 'id_econtracting_antigo', 'trabalha_atualmente_onu', 'trabalha_atualmente_opas'], 'integer'],
            [['qualificacao_info', 'publicacao'], 'string'],
            [['nome', 'sobrenome', 'email', 'url_blog', 'url_linkedin', 'url_site', 'twitter', 'url_facebook', 'skype', 'url_foto'], 'string', 'max' => 100],
            [['telefone_1', 'telefone_2'], 'string', 'max' => 45],
            [['nome_orgao_publico'], 'string', 'max' => 300],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
}
