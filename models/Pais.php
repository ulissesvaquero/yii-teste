<?php

namespace app\models;

use \app\models\base\Pais as BasePais;

/**
 * This is the model class for table "pais".
 */
class Pais extends BasePais
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['nm_code', 'is_ativo'], 'integer'],
            [['nome_pt', 'nome_en'], 'string', 'max' => 150],
            [['sigla'], 'string', 'max' => 2],
            [['sigla3'], 'string', 'max' => 3],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
}
