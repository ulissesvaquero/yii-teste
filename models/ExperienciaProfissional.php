<?php

namespace app\models;

use \app\models\base\ExperienciaProfissional as BaseExperienciaProfissional;

/**
 * This is the model class for table "experiencia_profissional".
 */
class ExperienciaProfissional extends BaseExperienciaProfissional
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['dt_inicio', 'dt_fim'], 'safe'],
            [['valor_salario_anual'], 'number'],
            [['tipo_salario_id', 'pessoa_id'], 'required'],
            [['tipo_salario_id', 'contacta_empregador', 'pessoa_id', 'is_emprego_atual', 'qtd_pessoa_supervisionou'], 'integer'],
            [['descricao_tarefa', 'descricao_resultado'], 'string'],
            [['nome_cargo'], 'string', 'max' => 150],
            [['nome_supervisor', 'nome_empregador', 'site_empresa'], 'string', 'max' => 200],
            [['descricao_motivo_saida'], 'string', 'max' => 100],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
}
