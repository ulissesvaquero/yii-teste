<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[FerramentaInformatica]].
 *
 * @see FerramentaInformatica
 */
class FerramentaInformaticaQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return FerramentaInformatica[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return FerramentaInformatica|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}