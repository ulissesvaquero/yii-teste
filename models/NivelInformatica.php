<?php

namespace app\models;

use \app\models\base\NivelInformatica as BaseNivelInformatica;

/**
 * This is the model class for table "nivel_informatica".
 */
class NivelInformatica extends BaseNivelInformatica
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['nome'], 'string', 'max' => 100],
            [['descricao'], 'string', 'max' => 200],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
}
