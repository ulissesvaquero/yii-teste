<?php

namespace app\models;

use \app\models\base\NivelIdioma as BaseNivelIdioma;

/**
 * This is the model class for table "nivel_idioma".
 */
class NivelIdioma extends BaseNivelIdioma
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['nome'], 'string', 'max' => 100],
            [['texto_oral', 'texto_leitura', 'texto_escrita'], 'string', 'max' => 300],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
}
