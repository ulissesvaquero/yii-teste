<?php

namespace app\models;

use \app\models\base\TipoFinanciamentoVaga as BaseTipoFinanciamentoVaga;

/**
 * This is the model class for table "tipo_financiamento_vaga".
 */
class TipoFinanciamentoVaga extends BaseTipoFinanciamentoVaga
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['nome'], 'string', 'max' => 100],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
}
