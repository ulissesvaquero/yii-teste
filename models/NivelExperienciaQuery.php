<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[NivelExperiencia]].
 *
 * @see NivelExperiencia
 */
class NivelExperienciaQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return NivelExperiencia[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return NivelExperiencia|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}