<?php

namespace app\models;

use \app\models\base\CompetenciaComportamental as BaseCompetenciaComportamental;

/**
 * This is the model class for table "competencia_comportamental".
 */
class CompetenciaComportamental extends BaseCompetenciaComportamental
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['tipo_competencia_comportamental_id'], 'required'],
            [['tipo_competencia_comportamental_id', 'id_antigo'], 'integer'],
            [['nome'], 'string', 'max' => 500],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
}
