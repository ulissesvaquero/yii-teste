<?php

namespace app\models;

use \app\models\base\Ut as BaseUt;

/**
 * This is the model class for table "ut".
 */
class Ut extends BaseUt
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['nome'], 'string', 'max' => 100],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
}
