<?php

namespace app\models;

use \app\models\base\ExperienciaProfissionalInternacional as BaseExperienciaProfissionalInternacional;

/**
 * This is the model class for table "experiencia_profissional_internacional".
 */
class ExperienciaProfissionalInternacional extends BaseExperienciaProfissionalInternacional
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['is_empregado', 'tipo_contrato_internacional_id', 'pessoa_id'], 'integer'],
            [['dt_inicio', 'dt_fim'], 'safe'],
            [['tipo_contrato_internacional_id', 'pessoa_id'], 'required'],
            [['nome_organizacao', 'lotacao'], 'string', 'max' => 200],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
}
