<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[FamiliarOnu]].
 *
 * @see FamiliarOnu
 */
class FamiliarOnuQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return FamiliarOnu[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return FamiliarOnu|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}