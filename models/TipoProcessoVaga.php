<?php

namespace app\models;

use \app\models\base\TipoProcessoVaga as BaseTipoProcessoVaga;

/**
 * This is the model class for table "tipo_processo_vaga".
 */
class TipoProcessoVaga extends BaseTipoProcessoVaga
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['nome'], 'string', 'max' => 100],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
}
