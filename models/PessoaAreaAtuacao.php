<?php

namespace app\models;

use \app\models\base\PessoaAreaAtuacao as BasePessoaAreaAtuacao;

/**
 * This is the model class for table "pessoa_area_atuacao".
 */
class PessoaAreaAtuacao extends BasePessoaAreaAtuacao
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['nivel_experiencia_id', 'especializacao_id', 'ano_experiencia_id', 'pessoa_id'], 'required'],
            [['nivel_experiencia_id', 'especializacao_id', 'ano_experiencia_id', 'pessoa_id'], 'integer'],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
}
