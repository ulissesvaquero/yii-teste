<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[TipoEdital]].
 *
 * @see TipoEdital
 */
class TipoEditalQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return TipoEdital[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return TipoEdital|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}