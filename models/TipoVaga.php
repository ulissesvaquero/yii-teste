<?php

namespace app\models;

use \app\models\base\TipoVaga as BaseTipoVaga;

/**
 * This is the model class for table "tipo_vaga".
 */
class TipoVaga extends BaseTipoVaga
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['nome'], 'string', 'max' => 45],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
}
