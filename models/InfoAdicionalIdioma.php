<?php

namespace app\models;

use \app\models\base\InfoAdicionalIdioma as BaseInfoAdicionalIdioma;

/**
 * This is the model class for table "info_adicional_idioma".
 */
class InfoAdicionalIdioma extends BaseInfoAdicionalIdioma
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['nome'], 'string', 'max' => 100],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
}
