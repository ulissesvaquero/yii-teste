<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use mootensai\behaviors\UUIDBehavior;

/**
 * This is the base model class for table "curso_formacao".
 *
 * @property integer $id
 * @property string $nome
 *
 * @property \app\models\OportunidadeCursoFormacao[] $oportunidadeCursoFormacaos
 * @property \app\models\Oportunidade[] $oportunidades
 * @property \app\models\PessoaEscolaridade[] $pessoaEscolaridades
 */
class CursoFormacao extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nome'], 'string', 'max' => 200],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'curso_formacao';
    }

    /**
     * 
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock 
     * 
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nome' => 'Nome',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOportunidadeCursoFormacaos()
    {
        return $this->hasMany(\app\models\OportunidadeCursoFormacao::className(), ['curso_formacao_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOportunidades()
    {
        return $this->hasMany(\app\models\Oportunidade::className(), ['id' => 'oportunidade_id'])->viaTable('oportunidade_curso_formacao', ['curso_formacao_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPessoaEscolaridades()
    {
        return $this->hasMany(\app\models\PessoaEscolaridade::className(), ['curso_formacao_id' => 'id']);
    }
    
/**
     * @inheritdoc
     * @return array mixed
     */ 
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'uuid' => [
                'class' => UUIDBehavior::className(),
                'column' => 'id',
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return \app\models\CursoFormacaoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\CursoFormacaoQuery(get_called_class());
    }
}
