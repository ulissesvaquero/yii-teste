<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use mootensai\behaviors\UUIDBehavior;

/**
 * This is the base model class for table "familiar_onu".
 *
 * @property integer $id
 * @property string $nome_parente
 * @property string $parentesco
 * @property string $agencia
 * @property string $md5_nome_parente
 * @property integer $tipo_contrato_internacional_id
 *
 * @property \app\models\TipoContratoInternacional $tipoContratoInternacional
 * @property \app\models\Pessoa[] $pessoas
 */
class FamiliarOnu extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tipo_contrato_internacional_id'], 'required'],
            [['tipo_contrato_internacional_id'], 'integer'],
            [['nome_parente', 'agencia'], 'string', 'max' => 200],
            [['parentesco'], 'string', 'max' => 100],
            [['md5_nome_parente'], 'string', 'max' => 150],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'familiar_onu';
    }

    /**
     * 
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock 
     * 
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nome_parente' => 'Nome Parente',
            'parentesco' => 'Parentesco',
            'agencia' => 'Agencia',
            'md5_nome_parente' => 'Md5 Nome Parente',
            'tipo_contrato_internacional_id' => 'Tipo Contrato Internacional ID',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTipoContratoInternacional()
    {
        return $this->hasOne(\app\models\TipoContratoInternacional::className(), ['id' => 'tipo_contrato_internacional_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPessoas()
    {
        return $this->hasMany(\app\models\Pessoa::className(), ['familiar_onu_id' => 'id']);
    }
    
/**
     * @inheritdoc
     * @return array mixed
     */ 
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'uuid' => [
                'class' => UUIDBehavior::className(),
                'column' => 'id',
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return \app\models\FamiliarOnuQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\FamiliarOnuQuery(get_called_class());
    }
}
