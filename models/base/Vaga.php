<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use mootensai\behaviors\UUIDBehavior;

/**
 * This is the base model class for table "vaga".
 *
 * @property integer $id
 * @property string $numero_posto
 * @property string $numero_processo
 * @property string $lotacao
 * @property integer $tipo_vaga_id
 * @property integer $tipo_processo_vaga_id
 * @property integer $tipo_duracao_vaga_id
 * @property integer $qtd_mes
 * @property integer $is_passivel_renovacao
 * @property integer $tipo_contrato_id
 * @property integer $tipo_moeda_salario_bruto_id
 * @property double $salario_bruto
 * @property string $subtarefa
 * @property string $objetivo_geral
 * @property string $beneficio
 * @property string $exp_essencial
 * @property string $exp_desejavel
 * @property string $resumo_responsabilidade
 * @property string $curso_formacao
 * @property string $nome_substituto_vaga
 * @property string $nome_outro_tipo_vaga
 * @property string $fonte_financiamento
 *
 * @property \app\models\Oportunidade[] $oportunidades
 * @property \app\models\TipoContrato $tipoContrato
 * @property \app\models\TipoDuracaoVaga $tipoDuracaoVaga
 * @property \app\models\TipoMoeda $tipoMoedaSalarioBruto
 * @property \app\models\TipoProcessoVaga $tipoProcessoVaga
 * @property \app\models\TipoVaga $tipoVaga
 */
class Vaga extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tipo_vaga_id', 'tipo_processo_vaga_id', 'tipo_duracao_vaga_id', 'qtd_mes', 'is_passivel_renovacao', 'tipo_contrato_id', 'tipo_moeda_salario_bruto_id'], 'integer'],
            [['tipo_processo_vaga_id', 'tipo_duracao_vaga_id', 'tipo_contrato_id', 'tipo_moeda_salario_bruto_id'], 'required'],
            [['salario_bruto'], 'number'],
            [['objetivo_geral', 'beneficio', 'exp_essencial', 'exp_desejavel', 'resumo_responsabilidade'], 'string'],
            [['numero_posto', 'numero_processo', 'fonte_financiamento'], 'string', 'max' => 100],
            [['lotacao', 'nome_outro_tipo_vaga'], 'string', 'max' => 45],
            [['subtarefa'], 'string', 'max' => 200],
            [['curso_formacao', 'nome_substituto_vaga'], 'string', 'max' => 300],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vaga';
    }

    /**
     * 
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock 
     * 
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'numero_posto' => 'Numero Posto',
            'numero_processo' => 'Numero Processo',
            'lotacao' => 'Lotacao',
            'tipo_vaga_id' => 'Tipo Vaga ID',
            'tipo_processo_vaga_id' => 'Tipo Processo Vaga ID',
            'tipo_duracao_vaga_id' => 'Tipo Duracao Vaga ID',
            'qtd_mes' => 'Qtd Mes',
            'is_passivel_renovacao' => 'Is Passivel Renovacao',
            'tipo_contrato_id' => 'Tipo Contrato ID',
            'tipo_moeda_salario_bruto_id' => 'Tipo Moeda Salario Bruto ID',
            'salario_bruto' => 'Salario Bruto',
            'subtarefa' => 'Subtarefa',
            'objetivo_geral' => 'Objetivo Geral',
            'beneficio' => 'Beneficio',
            'exp_essencial' => 'Exp Essencial',
            'exp_desejavel' => 'Exp Desejavel',
            'resumo_responsabilidade' => 'Resumo Responsabilidade',
            'curso_formacao' => 'Curso Formacao',
            'nome_substituto_vaga' => 'Nome Substituto Vaga',
            'nome_outro_tipo_vaga' => 'Nome Outro Tipo Vaga',
            'fonte_financiamento' => 'Fonte Financiamento',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOportunidades()
    {
        return $this->hasMany(\app\models\Oportunidade::className(), ['vaga_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTipoContrato()
    {
        return $this->hasOne(\app\models\TipoContrato::className(), ['id' => 'tipo_contrato_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTipoDuracaoVaga()
    {
        return $this->hasOne(\app\models\TipoDuracaoVaga::className(), ['id' => 'tipo_duracao_vaga_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTipoMoedaSalarioBruto()
    {
        return $this->hasOne(\app\models\TipoMoeda::className(), ['id' => 'tipo_moeda_salario_bruto_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTipoProcessoVaga()
    {
        return $this->hasOne(\app\models\TipoProcessoVaga::className(), ['id' => 'tipo_processo_vaga_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTipoVaga()
    {
        return $this->hasOne(\app\models\TipoVaga::className(), ['id' => 'tipo_vaga_id']);
    }
    
/**
     * @inheritdoc
     * @return array mixed
     */ 
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'uuid' => [
                'class' => UUIDBehavior::className(),
                'column' => 'id',
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return \app\models\VagaQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\VagaQuery(get_called_class());
    }
}
