<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use mootensai\behaviors\UUIDBehavior;

/**
 * This is the base model class for table "pessoa_escolaridade".
 *
 * @property integer $id
 * @property string $curso
 * @property string $nome_instituicao
 * @property string $dt_inicio
 * @property string $dt_fim
 * @property integer $situacao_curso_id
 * @property integer $escolaridade_id
 * @property string $estado
 * @property string $cidade
 * @property integer $pessoa_id
 * @property integer $pais_id
 * @property integer $curso_formacao_id
 *
 * @property \app\models\CursoFormacao $cursoFormacao
 * @property \app\models\Escolaridade $escolaridade
 * @property \app\models\Pais $pais
 * @property \app\models\Pessoa $pessoa
 * @property \app\models\SituacaoCurso $situacaoCurso
 */
class PessoaEscolaridade extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nome_instituicao', 'dt_inicio', 'situacao_curso_id', 'escolaridade_id', 'estado', 'cidade', 'pessoa_id', 'pais_id'], 'required'],
            [['dt_inicio', 'dt_fim'], 'safe'],
            [['situacao_curso_id', 'escolaridade_id', 'pessoa_id', 'pais_id', 'curso_formacao_id'], 'integer'],
            [['curso', 'estado', 'cidade'], 'string', 'max' => 300],
            [['nome_instituicao'], 'string', 'max' => 200],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pessoa_escolaridade';
    }

    /**
     * 
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock 
     * 
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'curso' => 'Curso',
            'nome_instituicao' => 'Nome Instituicao',
            'dt_inicio' => 'Dt Inicio',
            'dt_fim' => 'Dt Fim',
            'situacao_curso_id' => 'Situacao Curso ID',
            'escolaridade_id' => 'Escolaridade ID',
            'estado' => 'Estado',
            'cidade' => 'Cidade',
            'pessoa_id' => 'Pessoa ID',
            'pais_id' => 'Pais ID',
            'curso_formacao_id' => 'Curso Formacao ID',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCursoFormacao()
    {
        return $this->hasOne(\app\models\CursoFormacao::className(), ['id' => 'curso_formacao_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEscolaridade()
    {
        return $this->hasOne(\app\models\Escolaridade::className(), ['id' => 'escolaridade_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPais()
    {
        return $this->hasOne(\app\models\Pais::className(), ['id' => 'pais_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPessoa()
    {
        return $this->hasOne(\app\models\Pessoa::className(), ['id' => 'pessoa_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSituacaoCurso()
    {
        return $this->hasOne(\app\models\SituacaoCurso::className(), ['id' => 'situacao_curso_id']);
    }
    
/**
     * @inheritdoc
     * @return array mixed
     */ 
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'uuid' => [
                'class' => UUIDBehavior::className(),
                'column' => 'id',
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return \app\models\PessoaEscolaridadeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\PessoaEscolaridadeQuery(get_called_class());
    }
}
