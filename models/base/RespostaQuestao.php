<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use mootensai\behaviors\UUIDBehavior;

/**
 * This is the base model class for table "resposta_questao".
 *
 * @property integer $id
 * @property integer $questao_id
 * @property integer $resposta
 * @property integer $is_correta
 *
 * @property \app\models\Questao $questao
 */
class RespostaQuestao extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['questao_id'], 'required'],
            [['questao_id', 'resposta', 'is_correta'], 'integer'],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'resposta_questao';
    }

    /**
     * 
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock 
     * 
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'questao_id' => 'Questao ID',
            'resposta' => 'Resposta',
            'is_correta' => 'Is Correta',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestao()
    {
        return $this->hasOne(\app\models\Questao::className(), ['id' => 'questao_id']);
    }
    
/**
     * @inheritdoc
     * @return array mixed
     */ 
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'uuid' => [
                'class' => UUIDBehavior::className(),
                'column' => 'id',
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return \app\models\RespostaQuestaoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\RespostaQuestaoQuery(get_called_class());
    }
}
