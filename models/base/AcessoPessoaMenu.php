<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use mootensai\behaviors\UUIDBehavior;

/**
 * This is the base model class for table "acesso_pessoa_menu".
 *
 * @property integer $id
 * @property integer $menu_id
 * @property integer $pessoa_id
 * @property string $data_atualizacao
 * @property integer $is_informado
 *
 * @property \app\models\Menu $menu
 * @property \app\models\Pessoa $pessoa
 */
class AcessoPessoaMenu extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['menu_id', 'pessoa_id'], 'required'],
            [['menu_id', 'pessoa_id', 'is_informado'], 'integer'],
            [['data_atualizacao'], 'safe'],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'acesso_pessoa_menu';
    }

    /**
     * 
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock 
     * 
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'menu_id' => 'Menu ID',
            'pessoa_id' => 'Pessoa ID',
            'data_atualizacao' => 'Data Atualizacao',
            'is_informado' => 'Is Informado',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenu()
    {
        return $this->hasOne(\app\models\Menu::className(), ['id' => 'menu_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPessoa()
    {
        return $this->hasOne(\app\models\Pessoa::className(), ['id' => 'pessoa_id']);
    }
    
/**
     * @inheritdoc
     * @return array mixed
     */ 
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'uuid' => [
                'class' => UUIDBehavior::className(),
                'column' => 'id',
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return \app\models\AcessoPessoaMenuQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\AcessoPessoaMenuQuery(get_called_class());
    }
}
