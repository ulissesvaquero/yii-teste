<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use mootensai\behaviors\UUIDBehavior;

/**
 * This is the base model class for table "resposta_oportunidade_aplicada".
 *
 * @property integer $id
 * @property integer $oportunidade_aplicada_id
 * @property integer $is_correta
 * @property string $resposta
 * @property integer $questao_id
 *
 * @property \app\models\OportunidadeAplicada $oportunidadeAplicada
 * @property \app\models\Questao $questao
 */
class RespostaOportunidadeAplicada extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['oportunidade_aplicada_id', 'questao_id'], 'required'],
            [['oportunidade_aplicada_id', 'is_correta', 'questao_id'], 'integer'],
            [['resposta'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'resposta_oportunidade_aplicada';
    }

    /**
     * 
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock 
     * 
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'oportunidade_aplicada_id' => 'Oportunidade Aplicada ID',
            'is_correta' => 'Is Correta',
            'resposta' => 'Resposta',
            'questao_id' => 'Questao ID',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOportunidadeAplicada()
    {
        return $this->hasOne(\app\models\OportunidadeAplicada::className(), ['id' => 'oportunidade_aplicada_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestao()
    {
        return $this->hasOne(\app\models\Questao::className(), ['id' => 'questao_id']);
    }
    
/**
     * @inheritdoc
     * @return array mixed
     */ 
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'uuid' => [
                'class' => UUIDBehavior::className(),
                'column' => 'id',
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return \app\models\RespostaOportunidadeAplicadaQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\RespostaOportunidadeAplicadaQuery(get_called_class());
    }
}
