<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use mootensai\behaviors\UUIDBehavior;

/**
 * This is the base model class for table "referencia".
 *
 * @property integer $id
 * @property string $nome
 * @property string $endereco
 * @property string $email
 * @property string $telefone
 * @property string $cargo
 * @property string $empresa
 * @property string $tipo_vinculo
 * @property integer $contacta_referencia
 * @property integer $pessoa_id
 *
 * @property \app\models\Pessoa $pessoa
 */
class Referencia extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['contacta_referencia', 'pessoa_id'], 'integer'],
            [['pessoa_id'], 'required'],
            [['nome', 'email', 'telefone', 'cargo', 'empresa', 'tipo_vinculo'], 'string', 'max' => 100],
            [['endereco'], 'string', 'max' => 500],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'referencia';
    }

    /**
     * 
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock 
     * 
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nome' => 'Nome',
            'endereco' => 'Endereco',
            'email' => 'Email',
            'telefone' => 'Telefone',
            'cargo' => 'Cargo',
            'empresa' => 'Empresa',
            'tipo_vinculo' => 'Tipo Vinculo',
            'contacta_referencia' => 'Contacta Referencia',
            'pessoa_id' => 'Pessoa ID',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPessoa()
    {
        return $this->hasOne(\app\models\Pessoa::className(), ['id' => 'pessoa_id']);
    }
    
/**
     * @inheritdoc
     * @return array mixed
     */ 
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'uuid' => [
                'class' => UUIDBehavior::className(),
                'column' => 'id',
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return \app\models\ReferenciaQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\ReferenciaQuery(get_called_class());
    }
}
