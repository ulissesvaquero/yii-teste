<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use mootensai\behaviors\UUIDBehavior;

/**
 * This is the base model class for table "escolaridade".
 *
 * @property integer $id
 * @property string $nome
 *
 * @property \app\models\OportunidadeEscolaridade[] $oportunidadeEscolaridades
 * @property \app\models\Oportunidade[] $oportunidades
 * @property \app\models\PessoaEscolaridade[] $pessoaEscolaridades
 */
class Escolaridade extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nome'], 'string', 'max' => 100],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'escolaridade';
    }

    /**
     * 
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock 
     * 
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nome' => 'Nome',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOportunidadeEscolaridades()
    {
        return $this->hasMany(\app\models\OportunidadeEscolaridade::className(), ['escolaridade_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOportunidades()
    {
        return $this->hasMany(\app\models\Oportunidade::className(), ['id' => 'oportunidade_id'])->viaTable('oportunidade_escolaridade', ['escolaridade_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPessoaEscolaridades()
    {
        return $this->hasMany(\app\models\PessoaEscolaridade::className(), ['escolaridade_id' => 'id']);
    }
    
/**
     * @inheritdoc
     * @return array mixed
     */ 
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'uuid' => [
                'class' => UUIDBehavior::className(),
                'column' => 'id',
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return \app\models\EscolaridadeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\EscolaridadeQuery(get_called_class());
    }
}
