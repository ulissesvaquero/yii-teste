<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use mootensai\behaviors\UUIDBehavior;

/**
 * This is the base model class for table "nivel_idioma".
 *
 * @property integer $id
 * @property string $nome
 * @property string $texto_oral
 * @property string $texto_leitura
 * @property string $texto_escrita
 *
 * @property \app\models\OportunidadeIdioma[] $oportunidadeIdiomas
 * @property \app\models\PessoaIdioma[] $pessoaIdiomas
 */
class NivelIdioma extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nome'], 'string', 'max' => 100],
            [['texto_oral', 'texto_leitura', 'texto_escrita'], 'string', 'max' => 300],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'nivel_idioma';
    }

    /**
     * 
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock 
     * 
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nome' => 'Nome',
            'texto_oral' => 'Texto Oral',
            'texto_leitura' => 'Texto Leitura',
            'texto_escrita' => 'Texto Escrita',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOportunidadeIdiomas()
    {
        return $this->hasMany(\app\models\OportunidadeIdioma::className(), ['nivel_idioma_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPessoaIdiomas()
    {
        return $this->hasMany(\app\models\PessoaIdioma::className(), ['nivel_escrita' => 'id']);
    }
    
/**
     * @inheritdoc
     * @return array mixed
     */ 
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'uuid' => [
                'class' => UUIDBehavior::className(),
                'column' => 'id',
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return \app\models\NivelIdiomaQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\NivelIdiomaQuery(get_called_class());
    }
}
