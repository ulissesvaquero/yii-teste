<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use mootensai\behaviors\UUIDBehavior;

/**
 * This is the base model class for table "questao".
 *
 * @property integer $id
 * @property string $descricao
 * @property integer $tipo_questao_id
 * @property integer $oportunidade_id
 * @property string $gabarito
 *
 * @property \app\models\Oportunidade $oportunidade
 * @property \app\models\TipoQuestao $tipoQuestao
 * @property \app\models\RespostaOportunidadeAplicada[] $respostaOportunidadeAplicadas
 * @property \app\models\RespostaQuestao[] $respostaQuestaos
 */
class Questao extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['descricao'], 'string'],
            [['tipo_questao_id', 'oportunidade_id'], 'required'],
            [['tipo_questao_id', 'oportunidade_id'], 'integer'],
            [['gabarito'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'questao';
    }

    /**
     * 
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock 
     * 
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'descricao' => 'Descricao',
            'tipo_questao_id' => 'Tipo Questao ID',
            'oportunidade_id' => 'Oportunidade ID',
            'gabarito' => 'Gabarito',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOportunidade()
    {
        return $this->hasOne(\app\models\Oportunidade::className(), ['id' => 'oportunidade_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTipoQuestao()
    {
        return $this->hasOne(\app\models\TipoQuestao::className(), ['id' => 'tipo_questao_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRespostaOportunidadeAplicadas()
    {
        return $this->hasMany(\app\models\RespostaOportunidadeAplicada::className(), ['questao_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRespostaQuestaos()
    {
        return $this->hasMany(\app\models\RespostaQuestao::className(), ['questao_id' => 'id']);
    }
    
/**
     * @inheritdoc
     * @return array mixed
     */ 
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'uuid' => [
                'class' => UUIDBehavior::className(),
                'column' => 'id',
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return \app\models\QuestaoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\QuestaoQuery(get_called_class());
    }
}
