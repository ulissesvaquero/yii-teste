<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use mootensai\behaviors\UUIDBehavior;

/**
 * This is the base model class for table "oportunidade_especializacao".
 *
 * @property integer $oportunidade_id
 * @property integer $especializacao_id
 *
 * @property \app\models\Especializacao $especializacao
 * @property \app\models\Oportunidade $oportunidade
 */
class OportunidadeEspecializacao extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['oportunidade_id', 'especializacao_id'], 'required'],
            [['oportunidade_id', 'especializacao_id'], 'integer'],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'oportunidade_especializacao';
    }

    /**
     * 
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock 
     * 
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'oportunidade_id' => 'Oportunidade ID',
            'especializacao_id' => 'Especializacao ID',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEspecializacao()
    {
        return $this->hasOne(\app\models\Especializacao::className(), ['id' => 'especializacao_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOportunidade()
    {
        return $this->hasOne(\app\models\Oportunidade::className(), ['id' => 'oportunidade_id']);
    }
    
/**
     * @inheritdoc
     * @return array mixed
     */ 
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'uuid' => [
                'class' => UUIDBehavior::className(),
                'column' => 'id',
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return \app\models\OportunidadeEspecializacaoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\OportunidadeEspecializacaoQuery(get_called_class());
    }
}
