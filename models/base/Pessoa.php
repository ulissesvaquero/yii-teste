<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use mootensai\behaviors\UUIDBehavior;

/**
 * This is the base model class for table "pessoa".
 *
 * @property integer $id
 * @property string $nome
 * @property string $sobrenome
 * @property string $dt_nascimento
 * @property string $telefone_1
 * @property string $telefone_2
 * @property string $email
 * @property string $url_blog
 * @property string $url_linkedin
 * @property string $url_site
 * @property string $twitter
 * @property string $url_facebook
 * @property string $skype
 * @property string $url_foto
 * @property integer $titulo_id
 * @property integer $estado_civil_id
 * @property integer $sexo_id
 * @property integer $usuario_id
 * @property integer $endereco_id
 * @property integer $tipo_servico_publico_id
 * @property integer $vinculo_servico_publico
 * @property integer $tipo_dedicacao_id
 * @property string $nome_orgao_publico
 * @property integer $situacao_contratual_id
 * @property integer $familiar_onu_id
 * @property integer $id_econtracting_antigo
 * @property integer $trabalha_atualmente_onu
 * @property integer $trabalha_atualmente_opas
 * @property string $qualificacao_info
 * @property string $publicacao
 *
 * @property \app\models\AcessoPessoaMenu[] $acessoPessoaMenus
 * @property \app\models\Avaliador[] $avaliadors
 * @property \app\models\ExperienciaProfissional[] $experienciaProfissionals
 * @property \app\models\ExperienciaProfissionalInternacional[] $experienciaProfissionalInternacionals
 * @property \app\models\HabilidadeInformatica[] $habilidadeInformaticas
 * @property \app\models\OportunidadeAplicada[] $oportunidadeAplicadas
 * @property \app\models\Endereco $endereco
 * @property \app\models\EstadoCivil $estadoCivil
 * @property \app\models\FamiliarOnu $familiarOnu
 * @property \app\models\Sexo $sexo
 * @property \app\models\SituacaoContratual $situacaoContratual
 * @property \app\models\TipoDedicacao $tipoDedicacao
 * @property \app\models\TipoServicoPublico $tipoServicoPublico
 * @property \app\models\Titulo $titulo
 * @property \app\models\Usuario $usuario
 * @property \app\models\PessoaAreaAtuacao[] $pessoaAreaAtuacaos
 * @property \app\models\PessoaEscolaridade[] $pessoaEscolaridades
 * @property \app\models\PessoaIdioma[] $pessoaIdiomas
 * @property \app\models\PessoaNacionalidade[] $pessoaNacionalidades
 * @property \app\models\Pais[] $pais
 * @property \app\models\PessoaStorage[] $pessoaStorages
 * @property \app\models\PessoaTipoContrato[] $pessoaTipoContratos
 * @property \app\models\TipoContrato[] $tipoContratos
 * @property \app\models\Referencia[] $referencias
 */
class Pessoa extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['dt_nascimento'], 'safe'],
            [['titulo_id', 'estado_civil_id', 'sexo_id', 'usuario_id', 'endereco_id', 'tipo_servico_publico_id', 'vinculo_servico_publico', 'tipo_dedicacao_id', 'situacao_contratual_id', 'familiar_onu_id', 'id_econtracting_antigo', 'trabalha_atualmente_onu', 'trabalha_atualmente_opas'], 'integer'],
            [['qualificacao_info', 'publicacao'], 'string'],
            [['nome', 'sobrenome', 'email', 'url_blog', 'url_linkedin', 'url_site', 'twitter', 'url_facebook', 'skype', 'url_foto'], 'string', 'max' => 100],
            [['telefone_1', 'telefone_2'], 'string', 'max' => 45],
            [['nome_orgao_publico'], 'string', 'max' => 300],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pessoa';
    }

    /**
     * 
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock 
     * 
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nome' => 'Nome',
            'sobrenome' => 'Sobrenome',
            'dt_nascimento' => 'Dt Nascimento',
            'telefone_1' => 'Telefone 1',
            'telefone_2' => 'Telefone 2',
            'email' => 'Email',
            'url_blog' => 'Url Blog',
            'url_linkedin' => 'Url Linkedin',
            'url_site' => 'Url Site',
            'twitter' => 'Twitter',
            'url_facebook' => 'Url Facebook',
            'skype' => 'Skype',
            'url_foto' => 'Url Foto',
            'titulo_id' => 'Titulo ID',
            'estado_civil_id' => 'Estado Civil ID',
            'sexo_id' => 'Sexo ID',
            'usuario_id' => 'Usuario ID',
            'endereco_id' => 'Endereco ID',
            'tipo_servico_publico_id' => 'Tipo Servico Publico ID',
            'vinculo_servico_publico' => 'Vinculo Servico Publico',
            'tipo_dedicacao_id' => 'Tipo Dedicacao ID',
            'nome_orgao_publico' => 'Nome Orgao Publico',
            'situacao_contratual_id' => 'Situacao Contratual ID',
            'familiar_onu_id' => 'Familiar Onu ID',
            'id_econtracting_antigo' => 'Id Econtracting Antigo',
            'trabalha_atualmente_onu' => 'Trabalha Atualmente Onu',
            'trabalha_atualmente_opas' => 'Trabalha Atualmente Opas',
            'qualificacao_info' => 'Qualificacao Info',
            'publicacao' => 'Publicacao',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAcessoPessoaMenus()
    {
        return $this->hasMany(\app\models\AcessoPessoaMenu::className(), ['pessoa_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAvaliadors()
    {
        return $this->hasMany(\app\models\Avaliador::className(), ['pessoa_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExperienciaProfissionals()
    {
        return $this->hasMany(\app\models\ExperienciaProfissional::className(), ['pessoa_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExperienciaProfissionalInternacionals()
    {
        return $this->hasMany(\app\models\ExperienciaProfissionalInternacional::className(), ['pessoa_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHabilidadeInformaticas()
    {
        return $this->hasMany(\app\models\HabilidadeInformatica::className(), ['pessoa_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOportunidadeAplicadas()
    {
        return $this->hasMany(\app\models\OportunidadeAplicada::className(), ['pessoa_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEndereco()
    {
        return $this->hasOne(\app\models\Endereco::className(), ['id' => 'endereco_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEstadoCivil()
    {
        return $this->hasOne(\app\models\EstadoCivil::className(), ['id' => 'estado_civil_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFamiliarOnu()
    {
        return $this->hasOne(\app\models\FamiliarOnu::className(), ['id' => 'familiar_onu_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSexo()
    {
        return $this->hasOne(\app\models\Sexo::className(), ['id' => 'sexo_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSituacaoContratual()
    {
        return $this->hasOne(\app\models\SituacaoContratual::className(), ['id' => 'situacao_contratual_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTipoDedicacao()
    {
        return $this->hasOne(\app\models\TipoDedicacao::className(), ['id' => 'tipo_dedicacao_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTipoServicoPublico()
    {
        return $this->hasOne(\app\models\TipoServicoPublico::className(), ['id' => 'tipo_servico_publico_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTitulo()
    {
        return $this->hasOne(\app\models\Titulo::className(), ['id' => 'titulo_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario()
    {
        return $this->hasOne(\app\models\Usuario::className(), ['id' => 'usuario_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPessoaAreaAtuacaos()
    {
        return $this->hasMany(\app\models\PessoaAreaAtuacao::className(), ['pessoa_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPessoaEscolaridades()
    {
        return $this->hasMany(\app\models\PessoaEscolaridade::className(), ['pessoa_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPessoaIdiomas()
    {
        return $this->hasMany(\app\models\PessoaIdioma::className(), ['pessoa_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPessoaNacionalidades()
    {
        return $this->hasMany(\app\models\PessoaNacionalidade::className(), ['pessoa_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPais()
    {
        return $this->hasMany(\app\models\Pais::className(), ['id' => 'pais_id'])->viaTable('pessoa_nacionalidade', ['pessoa_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPessoaStorages()
    {
        return $this->hasMany(\app\models\PessoaStorage::className(), ['pessoa_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPessoaTipoContratos()
    {
        return $this->hasMany(\app\models\PessoaTipoContrato::className(), ['pessoa_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTipoContratos()
    {
        return $this->hasMany(\app\models\TipoContrato::className(), ['id' => 'tipo_contrato_id'])->viaTable('pessoa_tipo_contrato', ['pessoa_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReferencias()
    {
        return $this->hasMany(\app\models\Referencia::className(), ['pessoa_id' => 'id']);
    }
    
/**
     * @inheritdoc
     * @return array mixed
     */ 
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'uuid' => [
                'class' => UUIDBehavior::className(),
                'column' => 'id',
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return \app\models\PessoaQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\PessoaQuery(get_called_class());
    }
}
