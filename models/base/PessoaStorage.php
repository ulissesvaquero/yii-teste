<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use mootensai\behaviors\UUIDBehavior;

/**
 * This is the base model class for table "pessoa_storage".
 *
 * @property integer $id
 * @property integer $pessoa_id
 * @property integer $tipo_dado_storage_id
 * @property string $url
 * @property string $descricao
 *
 * @property \app\models\Pessoa $pessoa
 * @property \app\models\TipoDadoStorage $tipoDadoStorage
 */
class PessoaStorage extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pessoa_id', 'tipo_dado_storage_id'], 'required'],
            [['pessoa_id', 'tipo_dado_storage_id'], 'integer'],
            [['descricao'], 'string'],
            [['url'], 'string', 'max' => 500],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pessoa_storage';
    }

    /**
     * 
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock 
     * 
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'pessoa_id' => 'Pessoa ID',
            'tipo_dado_storage_id' => 'Tipo Dado Storage ID',
            'url' => 'Url',
            'descricao' => 'Descricao',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPessoa()
    {
        return $this->hasOne(\app\models\Pessoa::className(), ['id' => 'pessoa_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTipoDadoStorage()
    {
        return $this->hasOne(\app\models\TipoDadoStorage::className(), ['id' => 'tipo_dado_storage_id']);
    }
    
/**
     * @inheritdoc
     * @return array mixed
     */ 
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'uuid' => [
                'class' => UUIDBehavior::className(),
                'column' => 'id',
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return \app\models\PessoaStorageQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\PessoaStorageQuery(get_called_class());
    }
}
