<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use mootensai\behaviors\UUIDBehavior;

/**
 * This is the base model class for table "especializacao".
 *
 * @property integer $id
 * @property string $nome
 * @property integer $area_atuacao_id
 * @property integer $is_ativo
 * @property integer $id_econtracting_antigo
 *
 * @property \app\models\AreaAtuacao $areaAtuacao
 * @property \app\models\OportunidadeEspecializacao[] $oportunidadeEspecializacaos
 * @property \app\models\Oportunidade[] $oportunidades
 * @property \app\models\PessoaAreaAtuacao[] $pessoaAreaAtuacaos
 */
class Especializacao extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['area_atuacao_id'], 'required'],
            [['area_atuacao_id', 'is_ativo', 'id_econtracting_antigo'], 'integer'],
            [['nome'], 'string', 'max' => 200],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'especializacao';
    }

    /**
     * 
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock 
     * 
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nome' => 'Nome',
            'area_atuacao_id' => 'Area Atuacao ID',
            'is_ativo' => 'Is Ativo',
            'id_econtracting_antigo' => 'Id Econtracting Antigo',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAreaAtuacao()
    {
        return $this->hasOne(\app\models\AreaAtuacao::className(), ['id' => 'area_atuacao_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOportunidadeEspecializacaos()
    {
        return $this->hasMany(\app\models\OportunidadeEspecializacao::className(), ['especializacao_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOportunidades()
    {
        return $this->hasMany(\app\models\Oportunidade::className(), ['id' => 'oportunidade_id'])->viaTable('oportunidade_especializacao', ['especializacao_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPessoaAreaAtuacaos()
    {
        return $this->hasMany(\app\models\PessoaAreaAtuacao::className(), ['especializacao_id' => 'id']);
    }
    
/**
     * @inheritdoc
     * @return array mixed
     */ 
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'uuid' => [
                'class' => UUIDBehavior::className(),
                'column' => 'id',
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return \app\models\EspecializacaoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\EspecializacaoQuery(get_called_class());
    }
}
