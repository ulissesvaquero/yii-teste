<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use mootensai\behaviors\UUIDBehavior;

/**
 * This is the base model class for table "tipo_contrato".
 *
 * @property integer $id
 * @property string $nome
 *
 * @property \app\models\PessoaTipoContrato[] $pessoaTipoContratos
 * @property \app\models\Pessoa[] $pessoas
 * @property \app\models\Vaga[] $vagas
 */
class TipoContrato extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nome'], 'string', 'max' => 200],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tipo_contrato';
    }

    /**
     * 
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock 
     * 
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nome' => 'Nome',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPessoaTipoContratos()
    {
        return $this->hasMany(\app\models\PessoaTipoContrato::className(), ['tipo_contrato_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPessoas()
    {
        return $this->hasMany(\app\models\Pessoa::className(), ['id' => 'pessoa_id'])->viaTable('pessoa_tipo_contrato', ['tipo_contrato_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVagas()
    {
        return $this->hasMany(\app\models\Vaga::className(), ['tipo_contrato_id' => 'id']);
    }
    
/**
     * @inheritdoc
     * @return array mixed
     */ 
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'uuid' => [
                'class' => UUIDBehavior::className(),
                'column' => 'id',
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return \app\models\TipoContratoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\TipoContratoQuery(get_called_class());
    }
}
