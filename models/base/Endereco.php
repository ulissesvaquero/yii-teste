<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use mootensai\behaviors\UUIDBehavior;

/**
 * This is the base model class for table "endereco".
 *
 * @property integer $id
 * @property string $descricao
 * @property integer $pais_id
 * @property integer $uf_id
 * @property integer $cidade_id
 * @property string $bairro
 * @property string $cep
 * @property string $estado
 * @property string $cidade
 * @property string $md5_descricao
 *
 * @property \app\models\Cidade $cidade0
 * @property \app\models\Pais $pais
 * @property \app\models\Uf $uf
 * @property \app\models\Pessoa[] $pessoas
 */
class Endereco extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pais_id'], 'required'],
            [['pais_id', 'uf_id', 'cidade_id'], 'integer'],
            [['descricao', 'cep'], 'string', 'max' => 100],
            [['bairro', 'estado', 'cidade'], 'string', 'max' => 300],
            [['md5_descricao'], 'string', 'max' => 40],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'endereco';
    }

    /**
     * 
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock 
     * 
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'descricao' => 'Descricao',
            'pais_id' => 'Pais ID',
            'uf_id' => 'Uf ID',
            'cidade_id' => 'Cidade ID',
            'bairro' => 'Bairro',
            'cep' => 'Cep',
            'estado' => 'Estado',
            'cidade' => 'Cidade',
            'md5_descricao' => 'Md5 Descricao',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCidade0()
    {
        return $this->hasOne(\app\models\Cidade::className(), ['id' => 'cidade_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPais()
    {
        return $this->hasOne(\app\models\Pais::className(), ['id' => 'pais_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUf()
    {
        return $this->hasOne(\app\models\Uf::className(), ['id' => 'uf_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPessoas()
    {
        return $this->hasMany(\app\models\Pessoa::className(), ['endereco_id' => 'id']);
    }
    
/**
     * @inheritdoc
     * @return array mixed
     */ 
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'uuid' => [
                'class' => UUIDBehavior::className(),
                'column' => 'id',
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return \app\models\EnderecoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\EnderecoQuery(get_called_class());
    }
}
