<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use mootensai\behaviors\UUIDBehavior;

/**
 * This is the base model class for table "experiencia_profissional_internacional".
 *
 * @property integer $id
 * @property string $nome_organizacao
 * @property string $lotacao
 * @property integer $is_empregado
 * @property string $dt_inicio
 * @property string $dt_fim
 * @property integer $tipo_contrato_internacional_id
 * @property integer $pessoa_id
 *
 * @property \app\models\Pessoa $pessoa
 * @property \app\models\TipoContratoInternacional $tipoContratoInternacional
 */
class ExperienciaProfissionalInternacional extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['is_empregado', 'tipo_contrato_internacional_id', 'pessoa_id'], 'integer'],
            [['dt_inicio', 'dt_fim'], 'safe'],
            [['tipo_contrato_internacional_id', 'pessoa_id'], 'required'],
            [['nome_organizacao', 'lotacao'], 'string', 'max' => 200],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'experiencia_profissional_internacional';
    }

    /**
     * 
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock 
     * 
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nome_organizacao' => 'Nome Organizacao',
            'lotacao' => 'Lotacao',
            'is_empregado' => 'Is Empregado',
            'dt_inicio' => 'Dt Inicio',
            'dt_fim' => 'Dt Fim',
            'tipo_contrato_internacional_id' => 'Tipo Contrato Internacional ID',
            'pessoa_id' => 'Pessoa ID',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPessoa()
    {
        return $this->hasOne(\app\models\Pessoa::className(), ['id' => 'pessoa_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTipoContratoInternacional()
    {
        return $this->hasOne(\app\models\TipoContratoInternacional::className(), ['id' => 'tipo_contrato_internacional_id']);
    }
    
/**
     * @inheritdoc
     * @return array mixed
     */ 
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'uuid' => [
                'class' => UUIDBehavior::className(),
                'column' => 'id',
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return \app\models\ExperienciaProfissionalInternacionalQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\ExperienciaProfissionalInternacionalQuery(get_called_class());
    }
}
