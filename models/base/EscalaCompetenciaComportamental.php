<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use mootensai\behaviors\UUIDBehavior;

/**
 * This is the base model class for table "escala_competencia_comportamental".
 *
 * @property integer $id
 * @property string $nome
 * @property string $descricao
 * @property integer $competencia_comportamental_id
 *
 * @property \app\models\CompetenciaComportamental $competenciaComportamental
 * @property \app\models\OportunidadeEscalaCompetenciaComportamental[] $oportunidadeEscalaCompetenciaComportamentals
 * @property \app\models\Oportunidade[] $oportunidades
 */
class EscalaCompetenciaComportamental extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['descricao'], 'string'],
            [['competencia_comportamental_id'], 'required'],
            [['competencia_comportamental_id'], 'integer'],
            [['nome'], 'string', 'max' => 500],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'escala_competencia_comportamental';
    }

    /**
     * 
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock 
     * 
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nome' => 'Nome',
            'descricao' => 'Descricao',
            'competencia_comportamental_id' => 'Competencia Comportamental ID',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompetenciaComportamental()
    {
        return $this->hasOne(\app\models\CompetenciaComportamental::className(), ['id' => 'competencia_comportamental_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOportunidadeEscalaCompetenciaComportamentals()
    {
        return $this->hasMany(\app\models\OportunidadeEscalaCompetenciaComportamental::className(), ['escala_competencia_comportamental_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOportunidades()
    {
        return $this->hasMany(\app\models\Oportunidade::className(), ['id' => 'oportunidade_id'])->viaTable('oportunidade_escala_competencia_comportamental', ['escala_competencia_comportamental_id' => 'id']);
    }
    
/**
     * @inheritdoc
     * @return array mixed
     */ 
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'uuid' => [
                'class' => UUIDBehavior::className(),
                'column' => 'id',
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return \app\models\EscalaCompetenciaComportamentalQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\EscalaCompetenciaComportamentalQuery(get_called_class());
    }
}
