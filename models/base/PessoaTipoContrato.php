<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use mootensai\behaviors\UUIDBehavior;

/**
 * This is the base model class for table "pessoa_tipo_contrato".
 *
 * @property integer $pessoa_id
 * @property integer $tipo_contrato_id
 *
 * @property \app\models\Pessoa $pessoa
 * @property \app\models\TipoContrato $tipoContrato
 */
class PessoaTipoContrato extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pessoa_id', 'tipo_contrato_id'], 'required'],
            [['pessoa_id', 'tipo_contrato_id'], 'integer'],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pessoa_tipo_contrato';
    }

    /**
     * 
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock 
     * 
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pessoa_id' => 'Pessoa ID',
            'tipo_contrato_id' => 'Tipo Contrato ID',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPessoa()
    {
        return $this->hasOne(\app\models\Pessoa::className(), ['id' => 'pessoa_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTipoContrato()
    {
        return $this->hasOne(\app\models\TipoContrato::className(), ['id' => 'tipo_contrato_id']);
    }
    
/**
     * @inheritdoc
     * @return array mixed
     */ 
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'uuid' => [
                'class' => UUIDBehavior::className(),
                'column' => 'id',
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return \app\models\PessoaTipoContratoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\PessoaTipoContratoQuery(get_called_class());
    }
}
