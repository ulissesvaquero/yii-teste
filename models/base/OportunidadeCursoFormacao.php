<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use mootensai\behaviors\UUIDBehavior;

/**
 * This is the base model class for table "oportunidade_curso_formacao".
 *
 * @property integer $oportunidade_id
 * @property integer $curso_formacao_id
 *
 * @property \app\models\CursoFormacao $cursoFormacao
 * @property \app\models\Oportunidade $oportunidade
 */
class OportunidadeCursoFormacao extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['oportunidade_id', 'curso_formacao_id'], 'required'],
            [['oportunidade_id', 'curso_formacao_id'], 'integer'],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'oportunidade_curso_formacao';
    }

    /**
     * 
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock 
     * 
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'oportunidade_id' => 'Oportunidade ID',
            'curso_formacao_id' => 'Curso Formacao ID',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCursoFormacao()
    {
        return $this->hasOne(\app\models\CursoFormacao::className(), ['id' => 'curso_formacao_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOportunidade()
    {
        return $this->hasOne(\app\models\Oportunidade::className(), ['id' => 'oportunidade_id']);
    }
    
/**
     * @inheritdoc
     * @return array mixed
     */ 
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'uuid' => [
                'class' => UUIDBehavior::className(),
                'column' => 'id',
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return \app\models\OportunidadeCursoFormacaoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\OportunidadeCursoFormacaoQuery(get_called_class());
    }
}
