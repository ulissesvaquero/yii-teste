<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use mootensai\behaviors\UUIDBehavior;

/**
 * This is the base model class for table "oportunidade".
 *
 * @property integer $id
 * @property string $titulo
 * @property string $tipo_oportunidade
 * @property integer $edital_id
 * @property integer $vaga_id
 * @property string $data_inicio_divulgacao
 * @property string $data_fim_divulgacao
 * @property integer $pais_id
 * @property integer $cidade_id
 * @property integer $ut_id
 * @property string $nome_estado
 * @property string $nome_cidade
 * @property integer $uf_id
 * @property string $oficial_responsavel
 * @property string $data_publicacao
 * @property integer $fonte_financiamento_id
 * @property integer $status_oportunidade_id
 * @property string $competencia_tecnica
 *
 * @property \app\models\ArquivoOportunidadeFinalizada[] $arquivoOportunidadeFinalizadas
 * @property \app\models\Avaliador[] $avaliadors
 * @property \app\models\Cidade $cidade
 * @property \app\models\Edital $edital
 * @property \app\models\FonteFinanciamento $fonteFinanciamento
 * @property \app\models\Pais $pais
 * @property \app\models\StatusOportunidade $statusOportunidade
 * @property \app\models\Uf $uf
 * @property \app\models\Ut $ut
 * @property \app\models\Vaga $vaga
 * @property \app\models\OportunidadeAplicada[] $oportunidadeAplicadas
 * @property \app\models\OportunidadeCursoFormacao[] $oportunidadeCursoFormacaos
 * @property \app\models\CursoFormacao[] $cursoFormacaos
 * @property \app\models\OportunidadeEscalaCompetenciaComportamental[] $oportunidadeEscalaCompetenciaComportamentals
 * @property \app\models\EscalaCompetenciaComportamental[] $escalaCompetenciaComportamentals
 * @property \app\models\OportunidadeEscolaridade[] $oportunidadeEscolaridades
 * @property \app\models\Escolaridade[] $escolaridades
 * @property \app\models\OportunidadeEspecializacao[] $oportunidadeEspecializacaos
 * @property \app\models\Especializacao[] $especializacaos
 * @property \app\models\OportunidadeHabilidadeInformatica[] $oportunidadeHabilidadeInformaticas
 * @property \app\models\OportunidadeIdioma[] $oportunidadeIdiomas
 * @property \app\models\Questao[] $questaos
 */
class Oportunidade extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['edital_id', 'vaga_id', 'pais_id', 'cidade_id', 'ut_id', 'uf_id', 'fonte_financiamento_id', 'status_oportunidade_id'], 'integer'],
            [['data_inicio_divulgacao', 'data_fim_divulgacao', 'data_publicacao'], 'safe'],
            [['pais_id', 'fonte_financiamento_id', 'status_oportunidade_id'], 'required'],
            [['competencia_tecnica'], 'string'],
            [['titulo', 'nome_estado', 'nome_cidade', 'oficial_responsavel'], 'string', 'max' => 200],
            [['tipo_oportunidade'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'oportunidade';
    }

    /**
     * 
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock 
     * 
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'titulo' => 'Titulo',
            'tipo_oportunidade' => 'Tipo Oportunidade',
            'edital_id' => 'Edital ID',
            'vaga_id' => 'Vaga ID',
            'data_inicio_divulgacao' => 'Data Inicio Divulgacao',
            'data_fim_divulgacao' => 'Data Fim Divulgacao',
            'pais_id' => 'Pais ID',
            'cidade_id' => 'Cidade ID',
            'ut_id' => 'Ut ID',
            'nome_estado' => 'Nome Estado',
            'nome_cidade' => 'Nome Cidade',
            'uf_id' => 'Uf ID',
            'oficial_responsavel' => 'Oficial Responsavel',
            'data_publicacao' => 'Data Publicacao',
            'fonte_financiamento_id' => 'Fonte Financiamento ID',
            'status_oportunidade_id' => 'Status Oportunidade ID',
            'competencia_tecnica' => 'Competencia Tecnica',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArquivoOportunidadeFinalizadas()
    {
        return $this->hasMany(\app\models\ArquivoOportunidadeFinalizada::className(), ['oportunidade_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAvaliadors()
    {
        return $this->hasMany(\app\models\Avaliador::className(), ['oportunidade_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCidade()
    {
        return $this->hasOne(\app\models\Cidade::className(), ['id' => 'cidade_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEdital()
    {
        return $this->hasOne(\app\models\Edital::className(), ['id' => 'edital_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFonteFinanciamento()
    {
        return $this->hasOne(\app\models\FonteFinanciamento::className(), ['id' => 'fonte_financiamento_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPais()
    {
        return $this->hasOne(\app\models\Pais::className(), ['id' => 'pais_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatusOportunidade()
    {
        return $this->hasOne(\app\models\StatusOportunidade::className(), ['id' => 'status_oportunidade_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUf()
    {
        return $this->hasOne(\app\models\Uf::className(), ['id' => 'uf_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUt()
    {
        return $this->hasOne(\app\models\Ut::className(), ['id' => 'ut_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVaga()
    {
        return $this->hasOne(\app\models\Vaga::className(), ['id' => 'vaga_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOportunidadeAplicadas()
    {
        return $this->hasMany(\app\models\OportunidadeAplicada::className(), ['oportunidade_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOportunidadeCursoFormacaos()
    {
        return $this->hasMany(\app\models\OportunidadeCursoFormacao::className(), ['oportunidade_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCursoFormacaos()
    {
        return $this->hasMany(\app\models\CursoFormacao::className(), ['id' => 'curso_formacao_id'])->viaTable('oportunidade_curso_formacao', ['oportunidade_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOportunidadeEscalaCompetenciaComportamentals()
    {
        return $this->hasMany(\app\models\OportunidadeEscalaCompetenciaComportamental::className(), ['oportunidade_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEscalaCompetenciaComportamentals()
    {
        return $this->hasMany(\app\models\EscalaCompetenciaComportamental::className(), ['id' => 'escala_competencia_comportamental_id'])->viaTable('oportunidade_escala_competencia_comportamental', ['oportunidade_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOportunidadeEscolaridades()
    {
        return $this->hasMany(\app\models\OportunidadeEscolaridade::className(), ['oportunidade_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEscolaridades()
    {
        return $this->hasMany(\app\models\Escolaridade::className(), ['id' => 'escolaridade_id'])->viaTable('oportunidade_escolaridade', ['oportunidade_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOportunidadeEspecializacaos()
    {
        return $this->hasMany(\app\models\OportunidadeEspecializacao::className(), ['oportunidade_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEspecializacaos()
    {
        return $this->hasMany(\app\models\Especializacao::className(), ['id' => 'especializacao_id'])->viaTable('oportunidade_especializacao', ['oportunidade_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOportunidadeHabilidadeInformaticas()
    {
        return $this->hasMany(\app\models\OportunidadeHabilidadeInformatica::className(), ['oportunidade_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOportunidadeIdiomas()
    {
        return $this->hasMany(\app\models\OportunidadeIdioma::className(), ['oportunidade_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestaos()
    {
        return $this->hasMany(\app\models\Questao::className(), ['oportunidade_id' => 'id']);
    }
    
/**
     * @inheritdoc
     * @return array mixed
     */ 
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'uuid' => [
                'class' => UUIDBehavior::className(),
                'column' => 'id',
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return \app\models\OportunidadeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\OportunidadeQuery(get_called_class());
    }
}
