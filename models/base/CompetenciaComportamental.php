<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use mootensai\behaviors\UUIDBehavior;

/**
 * This is the base model class for table "competencia_comportamental".
 *
 * @property integer $id
 * @property string $nome
 * @property integer $tipo_competencia_comportamental_id
 * @property integer $id_antigo
 *
 * @property \app\models\TipoCompetenciaComportamental $tipoCompetenciaComportamental
 * @property \app\models\EscalaCompetenciaComportamental[] $escalaCompetenciaComportamentals
 */
class CompetenciaComportamental extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tipo_competencia_comportamental_id'], 'required'],
            [['tipo_competencia_comportamental_id', 'id_antigo'], 'integer'],
            [['nome'], 'string', 'max' => 500],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'competencia_comportamental';
    }

    /**
     * 
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock 
     * 
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nome' => 'Nome',
            'tipo_competencia_comportamental_id' => 'Tipo Competencia Comportamental ID',
            'id_antigo' => 'Id Antigo',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTipoCompetenciaComportamental()
    {
        return $this->hasOne(\app\models\TipoCompetenciaComportamental::className(), ['id' => 'tipo_competencia_comportamental_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEscalaCompetenciaComportamentals()
    {
        return $this->hasMany(\app\models\EscalaCompetenciaComportamental::className(), ['competencia_comportamental_id' => 'id']);
    }
    
/**
     * @inheritdoc
     * @return array mixed
     */ 
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'uuid' => [
                'class' => UUIDBehavior::className(),
                'column' => 'id',
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return \app\models\CompetenciaComportamentalQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\CompetenciaComportamentalQuery(get_called_class());
    }
}
