<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use mootensai\behaviors\UUIDBehavior;

/**
 * This is the base model class for table "oportunidade_escolaridade".
 *
 * @property integer $oportunidade_id
 * @property integer $escolaridade_id
 *
 * @property \app\models\Escolaridade $escolaridade
 * @property \app\models\Oportunidade $oportunidade
 */
class OportunidadeEscolaridade extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['oportunidade_id', 'escolaridade_id'], 'required'],
            [['oportunidade_id', 'escolaridade_id'], 'integer'],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'oportunidade_escolaridade';
    }

    /**
     * 
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock 
     * 
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'oportunidade_id' => 'Oportunidade ID',
            'escolaridade_id' => 'Escolaridade ID',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEscolaridade()
    {
        return $this->hasOne(\app\models\Escolaridade::className(), ['id' => 'escolaridade_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOportunidade()
    {
        return $this->hasOne(\app\models\Oportunidade::className(), ['id' => 'oportunidade_id']);
    }
    
/**
     * @inheritdoc
     * @return array mixed
     */ 
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'uuid' => [
                'class' => UUIDBehavior::className(),
                'column' => 'id',
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return \app\models\OportunidadeEscolaridadeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\OportunidadeEscolaridadeQuery(get_called_class());
    }
}
