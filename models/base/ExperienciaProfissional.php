<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use mootensai\behaviors\UUIDBehavior;

/**
 * This is the base model class for table "experiencia_profissional".
 *
 * @property integer $id
 * @property string $nome_cargo
 * @property string $dt_inicio
 * @property string $dt_fim
 * @property double $valor_salario_anual
 * @property integer $tipo_salario_id
 * @property string $nome_supervisor
 * @property string $nome_empregador
 * @property string $site_empresa
 * @property string $descricao_tarefa
 * @property string $descricao_resultado
 * @property string $descricao_motivo_saida
 * @property integer $contacta_empregador
 * @property integer $pessoa_id
 * @property integer $is_emprego_atual
 * @property integer $qtd_pessoa_supervisionou
 *
 * @property \app\models\Pessoa $pessoa
 * @property \app\models\TipoSalario $tipoSalario
 */
class ExperienciaProfissional extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['dt_inicio', 'dt_fim'], 'safe'],
            [['valor_salario_anual'], 'number'],
            [['tipo_salario_id', 'pessoa_id'], 'required'],
            [['tipo_salario_id', 'contacta_empregador', 'pessoa_id', 'is_emprego_atual', 'qtd_pessoa_supervisionou'], 'integer'],
            [['descricao_tarefa', 'descricao_resultado'], 'string'],
            [['nome_cargo'], 'string', 'max' => 150],
            [['nome_supervisor', 'nome_empregador', 'site_empresa'], 'string', 'max' => 200],
            [['descricao_motivo_saida'], 'string', 'max' => 100],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'experiencia_profissional';
    }

    /**
     * 
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock 
     * 
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nome_cargo' => 'Nome Cargo',
            'dt_inicio' => 'Dt Inicio',
            'dt_fim' => 'Dt Fim',
            'valor_salario_anual' => 'Valor Salario Anual',
            'tipo_salario_id' => 'Tipo Salario ID',
            'nome_supervisor' => 'Nome Supervisor',
            'nome_empregador' => 'Nome Empregador',
            'site_empresa' => 'Site Empresa',
            'descricao_tarefa' => 'Descricao Tarefa',
            'descricao_resultado' => 'Descricao Resultado',
            'descricao_motivo_saida' => 'Descricao Motivo Saida',
            'contacta_empregador' => 'Contacta Empregador',
            'pessoa_id' => 'Pessoa ID',
            'is_emprego_atual' => 'Is Emprego Atual',
            'qtd_pessoa_supervisionou' => 'Qtd Pessoa Supervisionou',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPessoa()
    {
        return $this->hasOne(\app\models\Pessoa::className(), ['id' => 'pessoa_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTipoSalario()
    {
        return $this->hasOne(\app\models\TipoSalario::className(), ['id' => 'tipo_salario_id']);
    }
    
/**
     * @inheritdoc
     * @return array mixed
     */ 
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'uuid' => [
                'class' => UUIDBehavior::className(),
                'column' => 'id',
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return \app\models\ExperienciaProfissionalQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\ExperienciaProfissionalQuery(get_called_class());
    }
}
