<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use mootensai\behaviors\UUIDBehavior;

/**
 * This is the base model class for table "pais".
 *
 * @property integer $id
 * @property string $nome_pt
 * @property string $nome_en
 * @property string $sigla
 * @property string $sigla3
 * @property integer $nm_code
 * @property integer $is_ativo
 *
 * @property \app\models\Endereco[] $enderecos
 * @property \app\models\Oportunidade[] $oportunidades
 * @property \app\models\PessoaEscolaridade[] $pessoaEscolaridades
 * @property \app\models\PessoaNacionalidade[] $pessoaNacionalidades
 * @property \app\models\Pessoa[] $pessoas
 */
class Pais extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nm_code', 'is_ativo'], 'integer'],
            [['nome_pt', 'nome_en'], 'string', 'max' => 150],
            [['sigla'], 'string', 'max' => 2],
            [['sigla3'], 'string', 'max' => 3],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pais';
    }

    /**
     * 
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock 
     * 
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nome_pt' => 'Nome Pt',
            'nome_en' => 'Nome En',
            'sigla' => 'Sigla',
            'sigla3' => 'Sigla3',
            'nm_code' => 'Nm Code',
            'is_ativo' => 'Is Ativo',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEnderecos()
    {
        return $this->hasMany(\app\models\Endereco::className(), ['pais_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOportunidades()
    {
        return $this->hasMany(\app\models\Oportunidade::className(), ['pais_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPessoaEscolaridades()
    {
        return $this->hasMany(\app\models\PessoaEscolaridade::className(), ['pais_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPessoaNacionalidades()
    {
        return $this->hasMany(\app\models\PessoaNacionalidade::className(), ['pais_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPessoas()
    {
        return $this->hasMany(\app\models\Pessoa::className(), ['id' => 'pessoa_id'])->viaTable('pessoa_nacionalidade', ['pais_id' => 'id']);
    }
    
/**
     * @inheritdoc
     * @return array mixed
     */ 
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'uuid' => [
                'class' => UUIDBehavior::className(),
                'column' => 'id',
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return \app\models\PaisQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\PaisQuery(get_called_class());
    }
}
