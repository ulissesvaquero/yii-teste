<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use mootensai\behaviors\UUIDBehavior;

/**
 * This is the base model class for table "nivel_informatica".
 *
 * @property integer $id
 * @property string $nome
 * @property string $descricao
 *
 * @property \app\models\HabilidadeInformatica[] $habilidadeInformaticas
 * @property \app\models\OportunidadeHabilidadeInformatica[] $oportunidadeHabilidadeInformaticas
 */
class NivelInformatica extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nome'], 'string', 'max' => 100],
            [['descricao'], 'string', 'max' => 200],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'nivel_informatica';
    }

    /**
     * 
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock 
     * 
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nome' => 'Nome',
            'descricao' => 'Descricao',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHabilidadeInformaticas()
    {
        return $this->hasMany(\app\models\HabilidadeInformatica::className(), ['nivel_informatica_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOportunidadeHabilidadeInformaticas()
    {
        return $this->hasMany(\app\models\OportunidadeHabilidadeInformatica::className(), ['nivel_informatica_id' => 'id']);
    }
    
/**
     * @inheritdoc
     * @return array mixed
     */ 
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'uuid' => [
                'class' => UUIDBehavior::className(),
                'column' => 'id',
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return \app\models\NivelInformaticaQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\NivelInformaticaQuery(get_called_class());
    }
}
