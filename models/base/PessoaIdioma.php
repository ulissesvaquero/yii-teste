<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use mootensai\behaviors\UUIDBehavior;

/**
 * This is the base model class for table "pessoa_idioma".
 *
 * @property integer $id
 * @property integer $pessoa_id
 * @property integer $nivel_oral
 * @property integer $nivel_leitura
 * @property integer $nivel_escrita
 * @property integer $idioma_id
 * @property integer $info_adicional_idioma_id
 *
 * @property \app\models\Idioma $idioma
 * @property \app\models\InfoAdicionalIdioma $infoAdicionalIdioma
 * @property \app\models\NivelIdioma $nivelOral
 * @property \app\models\NivelIdioma $nivelLeitura
 * @property \app\models\NivelIdioma $nivelEscrita
 * @property \app\models\Pessoa $pessoa
 */
class PessoaIdioma extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pessoa_id', 'nivel_oral', 'nivel_leitura', 'nivel_escrita', 'idioma_id', 'info_adicional_idioma_id'], 'required'],
            [['pessoa_id', 'nivel_oral', 'nivel_leitura', 'nivel_escrita', 'idioma_id', 'info_adicional_idioma_id'], 'integer'],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pessoa_idioma';
    }

    /**
     * 
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock 
     * 
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'pessoa_id' => 'Pessoa ID',
            'nivel_oral' => 'Nivel Oral',
            'nivel_leitura' => 'Nivel Leitura',
            'nivel_escrita' => 'Nivel Escrita',
            'idioma_id' => 'Idioma ID',
            'info_adicional_idioma_id' => 'Info Adicional Idioma ID',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdioma()
    {
        return $this->hasOne(\app\models\Idioma::className(), ['id' => 'idioma_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInfoAdicionalIdioma()
    {
        return $this->hasOne(\app\models\InfoAdicionalIdioma::className(), ['id' => 'info_adicional_idioma_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNivelOral()
    {
        return $this->hasOne(\app\models\NivelIdioma::className(), ['id' => 'nivel_oral']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNivelLeitura()
    {
        return $this->hasOne(\app\models\NivelIdioma::className(), ['id' => 'nivel_leitura']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNivelEscrita()
    {
        return $this->hasOne(\app\models\NivelIdioma::className(), ['id' => 'nivel_escrita']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPessoa()
    {
        return $this->hasOne(\app\models\Pessoa::className(), ['id' => 'pessoa_id']);
    }
    
/**
     * @inheritdoc
     * @return array mixed
     */ 
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'uuid' => [
                'class' => UUIDBehavior::className(),
                'column' => 'id',
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return \app\models\PessoaIdiomaQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\PessoaIdiomaQuery(get_called_class());
    }
}
