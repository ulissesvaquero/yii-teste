<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use mootensai\behaviors\UUIDBehavior;

/**
 * This is the base model class for table "oportunidade_aplicada".
 *
 * @property integer $id
 * @property string $data_aplicacacao
 * @property integer $pessoa_id
 * @property integer $oportunidade_id
 * @property integer $is_aprovado
 * @property double $score
 * @property string $carta_apresentacao
 * @property string $assinatura
 *
 * @property \app\models\Oportunidade $oportunidade
 * @property \app\models\Pessoa $pessoa
 * @property \app\models\RespostaOportunidadeAplicada[] $respostaOportunidadeAplicadas
 */
class OportunidadeAplicada extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['data_aplicacacao'], 'safe'],
            [['pessoa_id', 'oportunidade_id'], 'required'],
            [['pessoa_id', 'oportunidade_id', 'is_aprovado'], 'integer'],
            [['score'], 'number'],
            [['carta_apresentacao'], 'string'],
            [['assinatura'], 'string', 'max' => 500],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'oportunidade_aplicada';
    }

    /**
     * 
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock 
     * 
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'data_aplicacacao' => 'Data Aplicacacao',
            'pessoa_id' => 'Pessoa ID',
            'oportunidade_id' => 'Oportunidade ID',
            'is_aprovado' => 'Is Aprovado',
            'score' => 'Score',
            'carta_apresentacao' => 'Carta Apresentacao',
            'assinatura' => 'Assinatura',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOportunidade()
    {
        return $this->hasOne(\app\models\Oportunidade::className(), ['id' => 'oportunidade_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPessoa()
    {
        return $this->hasOne(\app\models\Pessoa::className(), ['id' => 'pessoa_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRespostaOportunidadeAplicadas()
    {
        return $this->hasMany(\app\models\RespostaOportunidadeAplicada::className(), ['oportunidade_aplicada_id' => 'id']);
    }
    
/**
     * @inheritdoc
     * @return array mixed
     */ 
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'uuid' => [
                'class' => UUIDBehavior::className(),
                'column' => 'id',
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return \app\models\OportunidadeAplicadaQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\OportunidadeAplicadaQuery(get_called_class());
    }
}
