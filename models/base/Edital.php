<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use mootensai\behaviors\UUIDBehavior;

/**
 * This is the base model class for table "edital".
 *
 * @property integer $id
 * @property integer $tempo_execucao
 * @property integer $tipo_edital_id
 * @property string $numero_edital
 * @property double $valor_total
 * @property string $antecedente_justificativa
 * @property string $objeto
 * @property string $metodologia_execucacao_servico
 * @property string $caminho_arquivo_modelo_proposta
 *
 * @property \app\models\TipoEdital $tipoEdital
 * @property \app\models\Oportunidade[] $oportunidades
 * @property \app\models\Produto[] $produtos
 */
class Edital extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tempo_execucao', 'tipo_edital_id'], 'integer'],
            [['tipo_edital_id'], 'required'],
            [['valor_total'], 'number'],
            [['antecedente_justificativa', 'objeto', 'metodologia_execucacao_servico'], 'string'],
            [['numero_edital'], 'string', 'max' => 45],
            [['caminho_arquivo_modelo_proposta'], 'string', 'max' => 300],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'edital';
    }

    /**
     * 
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock 
     * 
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tempo_execucao' => 'Tempo Execucao',
            'tipo_edital_id' => 'Tipo Edital ID',
            'numero_edital' => 'Numero Edital',
            'valor_total' => 'Valor Total',
            'antecedente_justificativa' => 'Antecedente Justificativa',
            'objeto' => 'Objeto',
            'metodologia_execucacao_servico' => 'Metodologia Execucacao Servico',
            'caminho_arquivo_modelo_proposta' => 'Caminho Arquivo Modelo Proposta',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTipoEdital()
    {
        return $this->hasOne(\app\models\TipoEdital::className(), ['id' => 'tipo_edital_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOportunidades()
    {
        return $this->hasMany(\app\models\Oportunidade::className(), ['edital_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProdutos()
    {
        return $this->hasMany(\app\models\Produto::className(), ['edital_id' => 'id']);
    }
    
/**
     * @inheritdoc
     * @return array mixed
     */ 
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'uuid' => [
                'class' => UUIDBehavior::className(),
                'column' => 'id',
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return \app\models\EditalQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\EditalQuery(get_called_class());
    }
}
