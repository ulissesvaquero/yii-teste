<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use mootensai\behaviors\UUIDBehavior;

/**
 * This is the base model class for table "fonte_financiamento".
 *
 * @property integer $id
 * @property string $cost_center
 * @property string $project_plan_task
 * @property string $initiative
 * @property string $project_hierarchy
 * @property string $fund
 * @property string $project
 * @property string $grantt
 *
 * @property \app\models\Oportunidade[] $oportunidades
 */
class FonteFinanciamento extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cost_center', 'project_plan_task', 'initiative', 'project_hierarchy', 'fund', 'project', 'grantt'], 'string', 'max' => 200],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'fonte_financiamento';
    }

    /**
     * 
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock 
     * 
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cost_center' => 'Cost Center',
            'project_plan_task' => 'Project Plan Task',
            'initiative' => 'Initiative',
            'project_hierarchy' => 'Project Hierarchy',
            'fund' => 'Fund',
            'project' => 'Project',
            'grantt' => 'Grantt',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOportunidades()
    {
        return $this->hasMany(\app\models\Oportunidade::className(), ['fonte_financiamento_id' => 'id']);
    }
    
/**
     * @inheritdoc
     * @return array mixed
     */ 
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'uuid' => [
                'class' => UUIDBehavior::className(),
                'column' => 'id',
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return \app\models\FonteFinanciamentoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\FonteFinanciamentoQuery(get_called_class());
    }
}
