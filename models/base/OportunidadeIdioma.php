<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use mootensai\behaviors\UUIDBehavior;

/**
 * This is the base model class for table "oportunidade_idioma".
 *
 * @property integer $id
 * @property integer $idioma_id
 * @property integer $nivel_idioma_id
 * @property integer $oportunidade_id
 *
 * @property \app\models\Idioma $idioma
 * @property \app\models\NivelIdioma $nivelIdioma
 * @property \app\models\Oportunidade $oportunidade
 */
class OportunidadeIdioma extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idioma_id', 'nivel_idioma_id', 'oportunidade_id'], 'required'],
            [['idioma_id', 'nivel_idioma_id', 'oportunidade_id'], 'integer'],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'oportunidade_idioma';
    }

    /**
     * 
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock 
     * 
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idioma_id' => 'Idioma ID',
            'nivel_idioma_id' => 'Nivel Idioma ID',
            'oportunidade_id' => 'Oportunidade ID',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdioma()
    {
        return $this->hasOne(\app\models\Idioma::className(), ['id' => 'idioma_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNivelIdioma()
    {
        return $this->hasOne(\app\models\NivelIdioma::className(), ['id' => 'nivel_idioma_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOportunidade()
    {
        return $this->hasOne(\app\models\Oportunidade::className(), ['id' => 'oportunidade_id']);
    }
    
/**
     * @inheritdoc
     * @return array mixed
     */ 
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'uuid' => [
                'class' => UUIDBehavior::className(),
                'column' => 'id',
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return \app\models\OportunidadeIdiomaQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\OportunidadeIdiomaQuery(get_called_class());
    }
}
