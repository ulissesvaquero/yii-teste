<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use mootensai\behaviors\UUIDBehavior;

/**
 * This is the base model class for table "produto".
 *
 * @property integer $id
 * @property integer $numero
 * @property string $descricao
 * @property string $data
 * @property double $valor
 * @property integer $edital_id
 *
 * @property \app\models\Edital $edital
 */
class Produto extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['numero', 'edital_id'], 'integer'],
            [['descricao'], 'string'],
            [['data'], 'safe'],
            [['valor'], 'number'],
            [['edital_id'], 'required'],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'produto';
    }

    /**
     * 
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock 
     * 
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'numero' => 'Numero',
            'descricao' => 'Descricao',
            'data' => 'Data',
            'valor' => 'Valor',
            'edital_id' => 'Edital ID',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEdital()
    {
        return $this->hasOne(\app\models\Edital::className(), ['id' => 'edital_id']);
    }
    
/**
     * @inheritdoc
     * @return array mixed
     */ 
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'uuid' => [
                'class' => UUIDBehavior::className(),
                'column' => 'id',
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return \app\models\ProdutoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\ProdutoQuery(get_called_class());
    }
}
