<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use mootensai\behaviors\UUIDBehavior;

/**
 * This is the base model class for table "area_atuacao".
 *
 * @property integer $id
 * @property string $nome
 * @property integer $is_ativo
 * @property integer $id_econtracting_antigo
 *
 * @property \app\models\Especializacao[] $especializacaos
 */
class AreaAtuacao extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['is_ativo', 'id_econtracting_antigo'], 'integer'],
            [['nome'], 'string', 'max' => 100],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'area_atuacao';
    }

    /**
     * 
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock 
     * 
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nome' => 'Nome',
            'is_ativo' => 'Is Ativo',
            'id_econtracting_antigo' => 'Id Econtracting Antigo',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEspecializacaos()
    {
        return $this->hasMany(\app\models\Especializacao::className(), ['area_atuacao_id' => 'id']);
    }
    
/**
     * @inheritdoc
     * @return array mixed
     */ 
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'uuid' => [
                'class' => UUIDBehavior::className(),
                'column' => 'id',
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return \app\models\AreaAtuacaoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\AreaAtuacaoQuery(get_called_class());
    }
}
