<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use mootensai\behaviors\UUIDBehavior;

/**
 * This is the base model class for table "oportunidade_habilidade_informatica".
 *
 * @property integer $id
 * @property integer $ferramenta_informatica_id
 * @property integer $nivel_informatica_id
 * @property integer $oportunidade_id
 *
 * @property \app\models\FerramentaInformatica $ferramentaInformatica
 * @property \app\models\NivelInformatica $nivelInformatica
 * @property \app\models\Oportunidade $oportunidade
 */
class OportunidadeHabilidadeInformatica extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ferramenta_informatica_id', 'nivel_informatica_id', 'oportunidade_id'], 'required'],
            [['ferramenta_informatica_id', 'nivel_informatica_id', 'oportunidade_id'], 'integer'],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'oportunidade_habilidade_informatica';
    }

    /**
     * 
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock 
     * 
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ferramenta_informatica_id' => 'Ferramenta Informatica ID',
            'nivel_informatica_id' => 'Nivel Informatica ID',
            'oportunidade_id' => 'Oportunidade ID',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFerramentaInformatica()
    {
        return $this->hasOne(\app\models\FerramentaInformatica::className(), ['id' => 'ferramenta_informatica_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNivelInformatica()
    {
        return $this->hasOne(\app\models\NivelInformatica::className(), ['id' => 'nivel_informatica_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOportunidade()
    {
        return $this->hasOne(\app\models\Oportunidade::className(), ['id' => 'oportunidade_id']);
    }
    
/**
     * @inheritdoc
     * @return array mixed
     */ 
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'uuid' => [
                'class' => UUIDBehavior::className(),
                'column' => 'id',
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return \app\models\OportunidadeHabilidadeInformaticaQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\OportunidadeHabilidadeInformaticaQuery(get_called_class());
    }
}
