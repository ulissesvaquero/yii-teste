<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use mootensai\behaviors\UUIDBehavior;

/**
 * This is the base model class for table "avaliador".
 *
 * @property integer $id
 * @property integer $pessoa_id
 * @property integer $tipo_avaliador_id
 * @property integer $oportunidade_id
 *
 * @property \app\models\Oportunidade $oportunidade
 * @property \app\models\TipoAvaliador $tipoAvaliador
 * @property \app\models\Pessoa $pessoa
 */
class Avaliador extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pessoa_id', 'tipo_avaliador_id', 'oportunidade_id'], 'required'],
            [['pessoa_id', 'tipo_avaliador_id', 'oportunidade_id'], 'integer'],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'avaliador';
    }

    /**
     * 
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock 
     * 
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'pessoa_id' => 'Pessoa ID',
            'tipo_avaliador_id' => 'Tipo Avaliador ID',
            'oportunidade_id' => 'Oportunidade ID',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOportunidade()
    {
        return $this->hasOne(\app\models\Oportunidade::className(), ['id' => 'oportunidade_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTipoAvaliador()
    {
        return $this->hasOne(\app\models\TipoAvaliador::className(), ['id' => 'tipo_avaliador_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPessoa()
    {
        return $this->hasOne(\app\models\Pessoa::className(), ['id' => 'pessoa_id']);
    }
    
/**
     * @inheritdoc
     * @return array mixed
     */ 
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'uuid' => [
                'class' => UUIDBehavior::className(),
                'column' => 'id',
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return \app\models\AvaliadorQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\AvaliadorQuery(get_called_class());
    }
}
