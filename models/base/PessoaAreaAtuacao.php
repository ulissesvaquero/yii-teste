<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use mootensai\behaviors\UUIDBehavior;

/**
 * This is the base model class for table "pessoa_area_atuacao".
 *
 * @property integer $id
 * @property integer $nivel_experiencia_id
 * @property integer $especializacao_id
 * @property integer $ano_experiencia_id
 * @property integer $pessoa_id
 *
 * @property \app\models\AnoExperiencia $anoExperiencia
 * @property \app\models\Especializacao $especializacao
 * @property \app\models\NivelExperiencia $nivelExperiencia
 * @property \app\models\Pessoa $pessoa
 */
class PessoaAreaAtuacao extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nivel_experiencia_id', 'especializacao_id', 'ano_experiencia_id', 'pessoa_id'], 'required'],
            [['nivel_experiencia_id', 'especializacao_id', 'ano_experiencia_id', 'pessoa_id'], 'integer'],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pessoa_area_atuacao';
    }

    /**
     * 
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock 
     * 
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nivel_experiencia_id' => 'Nivel Experiencia ID',
            'especializacao_id' => 'Especializacao ID',
            'ano_experiencia_id' => 'Ano Experiencia ID',
            'pessoa_id' => 'Pessoa ID',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAnoExperiencia()
    {
        return $this->hasOne(\app\models\AnoExperiencia::className(), ['id' => 'ano_experiencia_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEspecializacao()
    {
        return $this->hasOne(\app\models\Especializacao::className(), ['id' => 'especializacao_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNivelExperiencia()
    {
        return $this->hasOne(\app\models\NivelExperiencia::className(), ['id' => 'nivel_experiencia_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPessoa()
    {
        return $this->hasOne(\app\models\Pessoa::className(), ['id' => 'pessoa_id']);
    }
    
/**
     * @inheritdoc
     * @return array mixed
     */ 
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'uuid' => [
                'class' => UUIDBehavior::className(),
                'column' => 'id',
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return \app\models\PessoaAreaAtuacaoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\PessoaAreaAtuacaoQuery(get_called_class());
    }
}
