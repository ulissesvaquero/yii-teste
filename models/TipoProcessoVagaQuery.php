<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[TipoProcessoVaga]].
 *
 * @see TipoProcessoVaga
 */
class TipoProcessoVagaQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return TipoProcessoVaga[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return TipoProcessoVaga|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}