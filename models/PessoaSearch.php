<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Pessoa;

/**
 * app\models\PessoaSearch represents the model behind the search form about `app\models\Pessoa`.
 */
 class PessoaSearch extends Pessoa
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'titulo_id', 'estado_civil_id', 'sexo_id', 'usuario_id', 'endereco_id', 'tipo_servico_publico_id', 'vinculo_servico_publico', 'tipo_dedicacao_id', 'situacao_contratual_id', 'familiar_onu_id', 'id_econtracting_antigo', 'trabalha_atualmente_onu', 'trabalha_atualmente_opas'], 'integer'],
            [['nome', 'sobrenome', 'dt_nascimento', 'telefone_1', 'telefone_2', 'email', 'url_blog', 'url_linkedin', 'url_site', 'twitter', 'url_facebook', 'skype', 'url_foto', 'nome_orgao_publico', 'qualificacao_info', 'publicacao'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Pessoa::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'dt_nascimento' => $this->dt_nascimento,
            'titulo_id' => $this->titulo_id,
            'estado_civil_id' => $this->estado_civil_id,
            'sexo_id' => $this->sexo_id,
            'usuario_id' => $this->usuario_id,
            'endereco_id' => $this->endereco_id,
            'tipo_servico_publico_id' => $this->tipo_servico_publico_id,
            'vinculo_servico_publico' => $this->vinculo_servico_publico,
            'tipo_dedicacao_id' => $this->tipo_dedicacao_id,
            'situacao_contratual_id' => $this->situacao_contratual_id,
            'familiar_onu_id' => $this->familiar_onu_id,
            'id_econtracting_antigo' => $this->id_econtracting_antigo,
            'trabalha_atualmente_onu' => $this->trabalha_atualmente_onu,
            'trabalha_atualmente_opas' => $this->trabalha_atualmente_opas,
        ]);

        $query->andFilterWhere(['like', 'nome', $this->nome])
            ->andFilterWhere(['like', 'sobrenome', $this->sobrenome])
            ->andFilterWhere(['like', 'telefone_1', $this->telefone_1])
            ->andFilterWhere(['like', 'telefone_2', $this->telefone_2])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'url_blog', $this->url_blog])
            ->andFilterWhere(['like', 'url_linkedin', $this->url_linkedin])
            ->andFilterWhere(['like', 'url_site', $this->url_site])
            ->andFilterWhere(['like', 'twitter', $this->twitter])
            ->andFilterWhere(['like', 'url_facebook', $this->url_facebook])
            ->andFilterWhere(['like', 'skype', $this->skype])
            ->andFilterWhere(['like', 'url_foto', $this->url_foto])
            ->andFilterWhere(['like', 'nome_orgao_publico', $this->nome_orgao_publico])
            ->andFilterWhere(['like', 'qualificacao_info', $this->qualificacao_info])
            ->andFilterWhere(['like', 'publicacao', $this->publicacao]);

        return $dataProvider;
    }
}
