<?php

namespace app\models;

use \app\models\base\PessoaEscolaridade as BasePessoaEscolaridade;

/**
 * This is the model class for table "pessoa_escolaridade".
 */
class PessoaEscolaridade extends BasePessoaEscolaridade
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['nome_instituicao', 'dt_inicio', 'situacao_curso_id', 'escolaridade_id', 'estado', 'cidade', 'pessoa_id', 'pais_id'], 'required'],
            [['dt_inicio', 'dt_fim'], 'safe'],
            [['situacao_curso_id', 'escolaridade_id', 'pessoa_id', 'pais_id', 'curso_formacao_id'], 'integer'],
            [['curso', 'estado', 'cidade'], 'string', 'max' => 300],
            [['nome_instituicao'], 'string', 'max' => 200],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
}
