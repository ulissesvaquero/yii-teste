<?php

namespace app\models;

use \app\models\base\OportunidadeEscolaridade as BaseOportunidadeEscolaridade;

/**
 * This is the model class for table "oportunidade_escolaridade".
 */
class OportunidadeEscolaridade extends BaseOportunidadeEscolaridade
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['oportunidade_id', 'escolaridade_id'], 'required'],
            [['oportunidade_id', 'escolaridade_id'], 'integer'],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
}
