<?php

namespace app\models;

use \app\models\base\Edital as BaseEdital;

/**
 * This is the model class for table "edital".
 */
class Edital extends BaseEdital
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['tempo_execucao', 'tipo_edital_id'], 'integer'],
            [['tipo_edital_id'], 'required'],
            [['valor_total'], 'number'],
            [['antecedente_justificativa', 'objeto', 'metodologia_execucacao_servico'], 'string'],
            [['numero_edital'], 'string', 'max' => 45],
            [['caminho_arquivo_modelo_proposta'], 'string', 'max' => 300],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
}
