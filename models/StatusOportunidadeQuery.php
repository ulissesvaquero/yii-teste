<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[StatusOportunidade]].
 *
 * @see StatusOportunidade
 */
class StatusOportunidadeQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return StatusOportunidade[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return StatusOportunidade|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}