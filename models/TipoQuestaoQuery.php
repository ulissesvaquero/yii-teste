<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[TipoQuestao]].
 *
 * @see TipoQuestao
 */
class TipoQuestaoQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return TipoQuestao[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return TipoQuestao|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}