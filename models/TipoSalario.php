<?php

namespace app\models;

use \app\models\base\TipoSalario as BaseTipoSalario;

/**
 * This is the model class for table "tipo_salario".
 */
class TipoSalario extends BaseTipoSalario
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['nome'], 'string', 'max' => 100],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
}
