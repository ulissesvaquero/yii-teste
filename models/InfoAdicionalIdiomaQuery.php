<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[InfoAdicionalIdioma]].
 *
 * @see InfoAdicionalIdioma
 */
class InfoAdicionalIdiomaQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return InfoAdicionalIdioma[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return InfoAdicionalIdioma|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}