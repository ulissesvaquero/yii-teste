<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[OportunidadeEscalaCompetenciaComportamental]].
 *
 * @see OportunidadeEscalaCompetenciaComportamental
 */
class OportunidadeEscalaCompetenciaComportamentalQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return OportunidadeEscalaCompetenciaComportamental[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return OportunidadeEscalaCompetenciaComportamental|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}