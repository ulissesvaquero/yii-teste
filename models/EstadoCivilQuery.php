<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[EstadoCivil]].
 *
 * @see EstadoCivil
 */
class EstadoCivilQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return EstadoCivil[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return EstadoCivil|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}