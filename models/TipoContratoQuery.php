<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[TipoContrato]].
 *
 * @see TipoContrato
 */
class TipoContratoQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return TipoContrato[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return TipoContrato|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}