<?php

namespace app\models;

use \app\models\base\Referencia as BaseReferencia;

/**
 * This is the model class for table "referencia".
 */
class Referencia extends BaseReferencia
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['contacta_referencia', 'pessoa_id'], 'integer'],
            [['pessoa_id'], 'required'],
            [['nome', 'email', 'telefone', 'cargo', 'empresa', 'tipo_vinculo'], 'string', 'max' => 100],
            [['endereco'], 'string', 'max' => 500],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
}
