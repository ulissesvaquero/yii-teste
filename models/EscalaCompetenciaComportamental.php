<?php

namespace app\models;

use \app\models\base\EscalaCompetenciaComportamental as BaseEscalaCompetenciaComportamental;

/**
 * This is the model class for table "escala_competencia_comportamental".
 */
class EscalaCompetenciaComportamental extends BaseEscalaCompetenciaComportamental
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['descricao'], 'string'],
            [['competencia_comportamental_id'], 'required'],
            [['competencia_comportamental_id'], 'integer'],
            [['nome'], 'string', 'max' => 500],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
}
