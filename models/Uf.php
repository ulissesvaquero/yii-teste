<?php

namespace app\models;

use \app\models\base\Uf as BaseUf;

/**
 * This is the model class for table "uf".
 */
class Uf extends BaseUf
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['nome'], 'string', 'max' => 100],
            [['sigla'], 'string', 'max' => 2],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
}
