<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[HabilidadeInformatica]].
 *
 * @see HabilidadeInformatica
 */
class HabilidadeInformaticaQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return HabilidadeInformatica[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return HabilidadeInformatica|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}