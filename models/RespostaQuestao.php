<?php

namespace app\models;

use \app\models\base\RespostaQuestao as BaseRespostaQuestao;

/**
 * This is the model class for table "resposta_questao".
 */
class RespostaQuestao extends BaseRespostaQuestao
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['questao_id'], 'required'],
            [['questao_id', 'resposta', 'is_correta'], 'integer'],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
}
