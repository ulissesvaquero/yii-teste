<?php

namespace app\models;

use \app\models\base\CursoFormacao as BaseCursoFormacao;

/**
 * This is the model class for table "curso_formacao".
 */
class CursoFormacao extends BaseCursoFormacao
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['nome'], 'string', 'max' => 200],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
}
