<?php

namespace app\models;

use \app\models\base\OportunidadeHabilidadeInformatica as BaseOportunidadeHabilidadeInformatica;

/**
 * This is the model class for table "oportunidade_habilidade_informatica".
 */
class OportunidadeHabilidadeInformatica extends BaseOportunidadeHabilidadeInformatica
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['ferramenta_informatica_id', 'nivel_informatica_id', 'oportunidade_id'], 'required'],
            [['ferramenta_informatica_id', 'nivel_informatica_id', 'oportunidade_id'], 'integer'],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
}
