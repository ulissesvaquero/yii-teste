<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[ArquivoOportunidadeFinalizada]].
 *
 * @see ArquivoOportunidadeFinalizada
 */
class ArquivoOportunidadeFinalizadaQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return ArquivoOportunidadeFinalizada[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return ArquivoOportunidadeFinalizada|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}