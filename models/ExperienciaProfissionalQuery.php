<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[ExperienciaProfissional]].
 *
 * @see ExperienciaProfissional
 */
class ExperienciaProfissionalQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return ExperienciaProfissional[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return ExperienciaProfissional|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}