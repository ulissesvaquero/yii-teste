<?php

namespace app\models;

use \app\models\base\PessoaTipoContrato as BasePessoaTipoContrato;

/**
 * This is the model class for table "pessoa_tipo_contrato".
 */
class PessoaTipoContrato extends BasePessoaTipoContrato
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['pessoa_id', 'tipo_contrato_id'], 'required'],
            [['pessoa_id', 'tipo_contrato_id'], 'integer'],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
}
