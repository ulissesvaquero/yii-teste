<?php

namespace app\models;

use \app\models\base\AreaAtuacao as BaseAreaAtuacao;

/**
 * This is the model class for table "area_atuacao".
 */
class AreaAtuacao extends BaseAreaAtuacao
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['is_ativo', 'id_econtracting_antigo'], 'integer'],
            [['nome'], 'string', 'max' => 100],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
}
