<?php

namespace app\models;

use \app\models\base\OportunidadeCursoFormacao as BaseOportunidadeCursoFormacao;

/**
 * This is the model class for table "oportunidade_curso_formacao".
 */
class OportunidadeCursoFormacao extends BaseOportunidadeCursoFormacao
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['oportunidade_id', 'curso_formacao_id'], 'required'],
            [['oportunidade_id', 'curso_formacao_id'], 'integer'],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
}
