<?php

namespace app\models;

use \app\models\base\Endereco as BaseEndereco;

/**
 * This is the model class for table "endereco".
 */
class Endereco extends BaseEndereco
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['pais_id'], 'required'],
            [['pais_id', 'uf_id', 'cidade_id'], 'integer'],
            [['descricao', 'cep'], 'string', 'max' => 100],
            [['bairro', 'estado', 'cidade'], 'string', 'max' => 300],
            [['md5_descricao'], 'string', 'max' => 40],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
}
