<?php

namespace app\models;

use \app\models\base\HabilidadeInformatica as BaseHabilidadeInformatica;

/**
 * This is the model class for table "habilidade_informatica".
 */
class HabilidadeInformatica extends BaseHabilidadeInformatica
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['pessoa_id', 'ferramenta_informatica_id', 'nivel_informatica_id'], 'required'],
            [['pessoa_id', 'ferramenta_informatica_id', 'nivel_informatica_id'], 'integer'],
            [['descricao'], 'string'],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
}
