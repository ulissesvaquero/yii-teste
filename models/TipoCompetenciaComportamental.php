<?php

namespace app\models;

use \app\models\base\TipoCompetenciaComportamental as BaseTipoCompetenciaComportamental;

/**
 * This is the model class for table "tipo_competencia_comportamental".
 */
class TipoCompetenciaComportamental extends BaseTipoCompetenciaComportamental
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['nome'], 'string', 'max' => 200],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
}
