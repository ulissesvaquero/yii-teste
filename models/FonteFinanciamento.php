<?php

namespace app\models;

use \app\models\base\FonteFinanciamento as BaseFonteFinanciamento;

/**
 * This is the model class for table "fonte_financiamento".
 */
class FonteFinanciamento extends BaseFonteFinanciamento
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['cost_center', 'project_plan_task', 'initiative', 'project_hierarchy', 'fund', 'project', 'grantt'], 'string', 'max' => 200],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
}
