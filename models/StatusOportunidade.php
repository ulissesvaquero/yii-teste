<?php

namespace app\models;

use \app\models\base\StatusOportunidade as BaseStatusOportunidade;

/**
 * This is the model class for table "status_oportunidade".
 */
class StatusOportunidade extends BaseStatusOportunidade
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['nome'], 'string', 'max' => 100],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
}
