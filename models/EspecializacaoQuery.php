<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Especializacao]].
 *
 * @see Especializacao
 */
class EspecializacaoQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return Especializacao[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Especializacao|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}