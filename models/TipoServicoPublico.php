<?php

namespace app\models;

use \app\models\base\TipoServicoPublico as BaseTipoServicoPublico;

/**
 * This is the model class for table "tipo_servico_publico".
 */
class TipoServicoPublico extends BaseTipoServicoPublico
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['nome'], 'string', 'max' => 100],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
}
