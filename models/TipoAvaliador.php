<?php

namespace app\models;

use \app\models\base\TipoAvaliador as BaseTipoAvaliador;

/**
 * This is the model class for table "tipo_avaliador".
 */
class TipoAvaliador extends BaseTipoAvaliador
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['nome'], 'string', 'max' => 200],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
}
