<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[PessoaAreaAtuacao]].
 *
 * @see PessoaAreaAtuacao
 */
class PessoaAreaAtuacaoQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return PessoaAreaAtuacao[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return PessoaAreaAtuacao|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}